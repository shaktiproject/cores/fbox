////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Neel Gala, Sujay Pandit, Lokhesh Kumar
Email id: neelgala@gmail.com, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com
--------------------------------------------------------------------------------------------------
*/
package fpu_convert_hierarchical;
`include "Logger.bsv"
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import UniqueWrappers    ::*;
import FIFOF             ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

`ifdef fpu_hierarchical
 
`define Reg_width 32
`define ELEN 64
`define floating_output_dp_width 64
`define floating_output_sp_width 32
//// ------Contains:
// Compare
// Class
// int to sp
// sp to int 
// int to dp -> Incomplete
// dp to int 
////

////////// TYPEDEFS 
// For Class functions
typedef struct{
		Bit#(width) final_result;					// the final result for the operation
		Bit#(5) fflags; 					// indicates if any exception is generated.
}Floating_output#(numeric type width) deriving(Bits, Eq); // data structure of the output FIFO.
//
//

///////////////////////////// 
/// Dp to Sp or Sp to Dp
////////////////////////////
function Tuple2#(FloatingPoint#(e2,m2),Exception) fn_convert_spdp (Tuple3#(FloatingPoint#(e,m), RoundMode, Bool) operand)
   provisos(
      Max#(e, e2, emax),
      Max#(m, m2, mmax),
      Add#(emax, 1, expbits),
      Add#(mmax, 5, sfdbits),
      // per request of bsc
      Add#(a__, e, TAdd#(TMax#(e, e2), 1)),
      Add#(b__, e2, TAdd#(TMax#(e, e2), 1)),
      Add#(c__, TLog#(TAdd#(1, m)), expbits),
      Add#(d__, e2, expbits),
      Add#(m2, e__, sfdbits),
      Add#(f__, e, expbits),
      Add#(2, g__, sfdbits),
      Add#(1, TAdd#(1, TAdd#(m2, TAdd#(2, h__))), sfdbits),
      Add#(4, h__, e__),
      Add#(i__, TLog#(TAdd#(1, sfdbits)), TAdd#(e2, 1))
      );
      
   match {.in, .rmode, .preservenan} = operand;
   FloatingPoint#(e2,m2) out = defaultValue;
   Exception exc = defaultValue;

   if (isNaN(in)) begin
`ifdef denormal_support
      if (isSNaN(in) && !preservenan) begin
	 in = nanQuiet(in);
      end
`endif
      out.sign = False;
      out.exp = '1;
      out.sfd = zExtendLSB(2'b10);
//---------------------------------------------------------------------------------------------------
if(isSNaN(in))
      exc.invalid_op = True;
//---------------------------------------------------------------------------------------------------
//      exc.invalid_op = True;
`ifdef denormal_support
      Bit#(sfdbits) sfd = zExtendLSB(in.sfd);
      out.sfd = truncateLSB(sfd);
      if (out.sfd == 0)
	 out.sfd = zExtendLSB(2'b01);
`endif
   end
   else if (isInfinity(in))
      out = infinity(in.sign);
   else if (isZero(in))
      out = zero(in.sign);
   else if (isSubNormal(in)) begin
      Int#(expbits) exp = fromInteger(minexp(in));
      Bit#(sfdbits) sfd = zExtendLSB({1'b0, getHiddenBit(in),in.sfd});
      Int#(expbits) subexp = unpack(pack(extend(countZerosMSB(in.sfd))));

      if ((exp - subexp) > fromInteger(maxexp(out))) begin
	 out.sign = in.sign;
	 out.exp = maxBound - 1;
	 out.sfd = maxBound;

	 exc.overflow = True;
	 exc.inexact = True;

	 let y = round(rmode, out, '1);
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if ((exp - subexp) < fromInteger(minexp(out))) begin
	 out.sign = in.sign;
	 out.exp = 0;  // subnormal
`ifdef denormal_support
         //XXX XXX XXX Removed subnormal logic
	 let shift = fromInteger(abs(minexp(in)) - abs(minexp(out)));
	 if (shift < 0) begin
	    sfd = sfd << (-shift);
	 end
	 else if (shift < fromInteger(valueOf(sfdbits))) begin
	    Bit#(1) guard = |(sfd << (fromInteger(valueOf(sfdbits)) - shift));

	    sfd = sfd >> shift;
	    sfd[0] = sfd[0] | guard;
	 end
	 else if (|sfd == 1) begin
	    sfd = 1;
	 end

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);
`else
   out.sfd = 0;
`endif
	 if (isZero(out)) begin
	    exc.underflow = True;
	 end
`ifdef denormal_support
   let y = round(rmode, out, tpl_2(x));
`else
    let y = round(rmode, out, '0);
`endif
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
	 
      end
      else begin
	 out.sign = in.sign;
	 out.exp = pack(truncate(exp + fromInteger(bias(out))));

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);

	 let y = round(rmode, out, tpl_2(x));
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
     
      
   end
   else begin
      Int#(expbits) exp = signExtend(unpack(unbias(in)));
      Bit#(sfdbits) sfd = zExtendLSB({1'b0, getHiddenBit(in),in.sfd});

      if (exp > fromInteger(maxexp(out))) begin
	 out.sign = in.sign;
	 out.exp = maxBound - 1;
	 out.sfd = maxBound;

	 exc.overflow = True;
	 exc.inexact = True;

	 let y = round(rmode, out, '1);
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if (exp < fromInteger(minexp(out))) begin
	 out.sign = in.sign;
    out.exp = 0;  // subnormal
    
`ifdef denormal_support
         //XXX XXX XXX Removed subnormal logic
	 let shift = (fromInteger(minexp(out)) - exp);
	 if (shift < fromInteger(valueOf(sfdbits))) begin
	    Bit#(1) guard = |(sfd << (fromInteger(valueOf(sfdbits)) - shift));

	    sfd = sfd >> shift;
	    sfd[0] = sfd[0] | guard;
	 end
	 else if (|sfd == 1) begin
	    sfd = 1;
	 end

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);
`else
   out.sfd = 0;
`endif
	 if (isZero(out)) begin
	    exc.underflow = True;
	 end
`ifdef denormal_support
   let y = round(rmode, out, tpl_2(x));
`else
    let y = round(rmode, out, '0);
`endif
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
         
      end
      else begin
	 out.sign = in.sign;
	 out.exp = pack(truncate(exp + fromInteger(bias(out))));

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);

	 let y = round(rmode, out, tpl_2(x));
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
   end
//-------------------------------------------------------------------------------------------------------------

if(valueOf(e) == 'd11)
begin
	let temp = in.sfd;
	temp = temp >> 28;
	if(out.exp ==1 && out.sfd == 0 && in.exp == 'b01110000000 && temp == 'h0000000ffffff)
	exc.underflow= False;
	let temp1 = in.sfd;
	temp1 = temp1>>29;
	
	if((rmode == Rnd_Minus_Inf || rmode == Rnd_Plus_Inf) && out.exp ==1 && out.sfd == 0 && in.exp == 'b01110000000 && temp1 == 'h00000007fffff && in.sfd != 'hfffffe0000000)
	  exc.underflow = False;
end

//-------------------------------------------------------------------------------------------------------------

   return tuple2(canonicalize(out), exc);
endfunction

    interface Ifc_fpu_convert_sp_to_dp;
        method Action start(Tuple3#(FloatingPoint#(8,23), RoundMode, Bool) operand);
        method ReturnType#(11,52) receive();
    endinterface

    interface Ifc_fpu_convert_dp_to_sp;
        method Action start(Tuple3#(FloatingPoint#(11,52), RoundMode, Bool) operand);
        method ReturnType#(8,23) receive();
    endinterface

    (*synthesize*)
    module mk_fpu_convert_sp_to_dp_hierarchical(Ifc_fpu_convert_sp_to_dp);
        Vector#(`STAGES_FCONV,Reg#(Tuple2#(FloatingPoint#(11,52),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
        Vector#(`STAGES_FCONV,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
        rule rl_pipeline;
            for(Integer i = 1 ; i <= `STAGES_FCONV -1 ; i = i+1)
            begin
                rg_stage_out[i] <= rg_stage_out[i-1];
                rg_stage_valid[i] <= rg_stage_valid[i-1];
            end
        endrule
        method Action start(Tuple3#(FloatingPoint#(8,23), RoundMode, Bool) operand);
                    rg_stage_out[0] <= fn_convert_spdp(operand);
                    rg_stage_valid[0] <= 1;
        endmethod
        method ReturnType#(11,52) receive();
        let x = ReturnType{valid:rg_stage_valid[`STAGES_FCONV-1] ,value:tpl_1(rg_stage_out[`STAGES_FCONV-1]) ,ex:tpl_2(rg_stage_out[`STAGES_FCONV-1])};
        return x;
        endmethod 
    endmodule
    //
    (*synthesize*)
    module mk_fpu_convert_dp_to_sp_hierarchical(Ifc_fpu_convert_dp_to_sp);
        Vector#(`STAGES_FCONV,Reg#(Tuple2#(FloatingPoint#(8,23),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
        Vector#(`STAGES_FCONV,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
        rule rl_pipeline;
            for(Integer i = 1 ; i <= `STAGES_FCONV -1 ; i = i+1)
            begin
                rg_stage_out[i] <= rg_stage_out[i-1];
                rg_stage_valid[i] <= rg_stage_valid[i-1];
            end
        endrule
        method Action start(Tuple3#(FloatingPoint#(11,52), RoundMode, Bool) operand);
                    rg_stage_out[0] <= fn_convert_spdp(operand);
                    rg_stage_valid[0] <= 1;
        endmethod
        method ReturnType#(8,23) receive();
        let x = ReturnType{valid:rg_stage_valid[`STAGES_FCONV-1] ,value:tpl_1(rg_stage_out[`STAGES_FCONV-1]) ,ex:tpl_2(rg_stage_out[`STAGES_FCONV-1])};
        return x;
        endmethod 
    endmodule


///////////////////////////////
/// sp/dp to int (parameterised for values of 'e' and 'm')
//////////////////////////////

    interface Ifc_fpu_sp_to_int;
        method Action start(Tuple3#(FloatingPoint#(8,23),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
        method ReturnTypeInt#(`ELEN) receive();
    endinterface
    //
    interface Ifc_fpu_dp_to_int;
        method Action start(Tuple3#(FloatingPoint#(11,52),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
        method ReturnTypeInt#(`ELEN) receive();
    endinterface

    (*synthesize*)
    module mk_fpu_sp_to_int_hierarchical(Ifc_fpu_sp_to_int);
        Vector#(`STAGES_FCONV_FP_TO_INT,Reg#(Floating_output#(`ELEN))) rg_stage_out <- replicateM(mkReg(unpack(0)));
        Vector#(`STAGES_FCONV_FP_TO_INT,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
        rule rl_pipeline;
            for(Integer i = 1 ; i <= `STAGES_FCONV_FP_TO_INT -1 ; i = i+1)
            begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
            end
        endrule
        method Action start(Tuple3#(FloatingPoint#(8,23),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
            match {.in,.roundmode,.exc} = operands;
            let lv_sign = pack(in.sign);
            Integer lv_m_val = 'd23;
            Integer lv_e_val = 'd8;
            Bit#(8) lv_exponent = in.exp;
            Bit#(23) lv_mantissa = in.sfd;
            let rounding_mode = roundmode;
            let flags = pack(exc);
            bit lv_overflow = 0;
            bit lv_zero = flags[3];
            bit lv_infinity = flags[1];
            bit lv_invalid = flags[0] | flags[2];
            bit lv_denormal = flags[4];
            bit lv_manzero = |lv_mantissa;
            bit lv_inexact = 0;
            Bool to_round = False; 
            Bool rne = (rounding_mode == Rnd_Nearest_Even);
            Bool rtz = (rounding_mode == Rnd_Zero);
            Bool rdn = (rounding_mode == Rnd_Minus_Inf);
            Bool rup = (rounding_mode == Rnd_Plus_Inf);
            Bool rmm = (rounding_mode == Rnd_Nearest_Away_Zero);
            
            let lv_e_val_1 = lv_e_val+1;
            Bit#(9) lv_exp = {1'b0,lv_exponent};
            Int#(8) lv_original_exponent;

            lv_original_exponent = unpack(truncate(lv_exp - 127));  // removing the bias
            Bit#(`ELEN) final_result = 0;
            Bit#(TAdd#(23, `ELEN)) final_man = {'0,1'b1,lv_mantissa}; //parametrised for 'm' value
            if(lv_zero == 1)
                final_result = 0;
            else if(lv_denormal == 1 || (lv_original_exponent <= -1 && (lv_infinity|lv_invalid) == 0)) begin
                if(lv_sign==1 && convert_unsigned==1 && ((lv_original_exponent==-1 && (rmm||(rne && lv_manzero==1))) || (lv_original_exponent<=-1 &&rdn)))
                    lv_invalid = 1;
                else
                lv_inexact = 1;
                if(lv_sign == 0 && rup)
                    final_result = 1;
                else if(rdn && lv_sign == 1 && convert_unsigned == 0)
                    final_result = '1;
                else if(lv_original_exponent == -1 && (rmm||(rne && lv_manzero == 1)))begin
                    if(lv_sign == 0)
                        final_result = 1;
                    else if(convert_unsigned == 0)
                        final_result = '1;
                    else
                        final_result = 0;
                end
                else
                    final_result = 0;
            end
            else if(convert_long == 0) 
            begin         //FCVT.W.S FCVT.WU.S or  //FCVT.W.D FCVT.WU.D
                if(convert_unsigned == 0) //FCVT.W.S or //FCVT.W.D
                begin 
                    Bit#(31) all_ones = '1;
                    if(lv_infinity == 1 || lv_invalid == 1) 
                    begin
                    final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(32'h80000000)) : zeroExtend(all_ones); 
                    end
                    else if(lv_original_exponent < 'd31) 
                    begin
                    final_man = final_man << lv_original_exponent;
                    Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                    final_result = signExtend(y);
                    lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value 
                    to_round = True;
                    end
                    else if(lv_original_exponent >= 'd31) 
                    begin
                        lv_invalid = 1;
                        if(lv_sign == 0)
                        final_result = zeroExtend(all_ones);
                        else 
                        begin
                        if(lv_original_exponent == 'd31 && lv_manzero == 0)
                        lv_invalid = 0 ;        //Since we are exactly representing the number? 
                        final_result = signExtend(32'h80000000);
                        end
                    end
                end
                else begin     //FCVT.WU.S or //FCVT.WU.D
                Bit#(32) all_ones = '1;
                    if(lv_infinity == 1 || lv_invalid == 1)
                        final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
                    else if(lv_original_exponent < 'd32) begin
                        final_man = final_man << lv_original_exponent;
                        Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                        final_result = signExtend(y);
                        lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value
                        to_round = True;
                    end
                    else if(lv_original_exponent >= 'd32) begin
                        lv_invalid = 1;
                        if(lv_sign == 0)
                            final_result = signExtend(all_ones);
                        else
                            final_result = '0;
                    end
                end
            end
            `ifdef RV64
                else begin
                    if(convert_unsigned == 0) begin //FCVT.L.S or //FCVT.L.D
                        Bit#(63) all_ones = '1;
                        if(lv_infinity == 1 || lv_invalid == 1)
                            final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(64'h8000000000000000)) : zeroExtend(all_ones); 
                        else if(lv_original_exponent < 'd63) begin
                            final_man = final_man << lv_original_exponent;
                            Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                            final_result = zeroExtend(y);
                            lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                            to_round = True;
                        end
                        else if(lv_original_exponent >= 'd63) begin
                            lv_invalid = 1;
                            if(lv_sign == 0)
                                final_result = zeroExtend(all_ones);
                            else begin
                                if(lv_original_exponent == 'd63 && lv_manzero == 0 )
                                    lv_invalid = 0;  //Since we are exactly representing the input number
                                final_result = signExtend(64'h8000000000000000);
                            end
                        end
                    end
                    else begin     //FCVT.LU.S or //FCVT.LU.D
                        Bit#(64) all_ones = '1;
                        if(lv_infinity == 1 || lv_invalid == 1)
                            final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
                        else if(lv_original_exponent < 'd64) begin
                            final_man = final_man << lv_original_exponent;
                            Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                            final_result = zeroExtend(y);
                            lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                            to_round = True;
                        end
                        else if(lv_original_exponent >= 'd64) begin
                            lv_invalid = 1;
                            if(lv_sign == 0)
                                final_result = signExtend(all_ones);
                            else
                                final_result = '0;
                        end
                    end
                end
            `endif 

            bit lv_guard = lv_mantissa[lv_m_val-1];	        //MSB of the already shifted mantissa is guard bit //parametrised for 'm' value
            bit lv_round = lv_mantissa[lv_m_val-2];	        //next bit is round bit //parametrised for 'm' value
            bit lv_sticky = |(lv_mantissa<<2);		//remaining bits determine the sticky bit
            bit lv_round_up = 0;
            bit lv_inexact1 = lv_guard | lv_round | lv_sticky;
            if(to_round) begin
                if(rounding_mode == Rnd_Nearest_Even) 		
                lv_round_up = lv_guard & (final_result[0] | lv_round | lv_sticky);	//Round to nearest ties to even
                else if(rmm) 
                lv_round_up = lv_guard; //& (lv_round | lv_sticky | ~lv_sign);			//Round to nearest ties to max magnitude
                else if(rdn) 
                lv_round_up = lv_inexact1 & (lv_sign);								//Round down to -infinity
                else if(rup) 
                lv_round_up = lv_inexact1 & (~lv_sign);								//Round up to +infinity
                lv_inexact = lv_inexact | lv_inexact1;
                if(lv_round_up == 1) begin //Should set the overflow flag here right? 
                lv_invalid = 1;
                    if(convert_long == 0 && convert_unsigned == 0 && lv_original_exponent == 30 && 
                    final_result[30:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
                        final_result = 'h7fffffff; 
                    else if(convert_long == 0 && convert_unsigned == 1 && lv_original_exponent == 31 && final_result[31:0] == '1 && lv_sign == 0)
                        final_result = '1; 
                    `ifdef RV64
                        else if(convert_long == 1 && convert_unsigned == 0 && lv_original_exponent == 62 && final_result[62:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
                            final_result = 64'h7fffffffffffffff;
                        else if(convert_long == 1 && convert_unsigned == 1 && lv_original_exponent == 63 && final_result[63:0] == '1 && lv_sign == 0)
                            final_result = 64'hffffffffffffffff;                
                    `endif
                    else begin
                    lv_invalid = 0;
                    final_result = final_result + 1;
                    if(convert_long == 0 && final_result[31]==1)
                        final_result = signExtend(final_result[31:0]);
                    end
                end
                if(convert_unsigned == 0 && lv_sign == 1)begin		//Negating the output if floating point number is negative and converted to signed word/long
                    final_result = ~final_result + 1;
                    if(convert_long == 0 && final_result[31] == 1)
                        final_result = signExtend(final_result[31:0]);
                end
                else if(convert_unsigned == 1 && lv_sign == 1) begin
                    final_result = 0;
                    lv_invalid = 1;
                end
            end
        if((lv_invalid|lv_infinity) == 1) begin  //What about Quiet NaN?? What does the Spec Say?
            lv_overflow = 0;
            lv_inexact = 0;
        end
        Bit#(5) fflags={lv_invalid|lv_infinity,1'b0,lv_overflow,1'b0,lv_inexact};
        rg_stage_out[0] <= Floating_output{
                            final_result: final_result,
                            fflags: fflags};
        rg_stage_valid[0] <= 1;        
        endmethod
        method ReturnTypeInt#(`ELEN) receive();
            return ReturnTypeInt{valid:rg_stage_valid[`STAGES_FCONV_FP_TO_INT-1],value:rg_stage_out[`STAGES_FCONV_FP_TO_INT-1].final_result,ex:unpack(rg_stage_out[`STAGES_FCONV_FP_TO_INT-1].fflags)};
        endmethod 
    endmodule
    //
    (*synthesize*)
    module mk_fpu_dp_to_int_hierarchical(Ifc_fpu_dp_to_int);
        Vector#(`STAGES_FCONV_FP_TO_INT,Reg#(Floating_output#(`ELEN))) rg_stage_out <- replicateM(mkReg(unpack(0)));
        Vector#(`STAGES_FCONV_FP_TO_INT,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
        rule rl_pipeline;
            for(Integer i = 1 ; i <= `STAGES_FCONV_FP_TO_INT -1 ; i = i+1)
            begin
                rg_stage_out[i] <= rg_stage_out[i-1];
                rg_stage_valid[i] <= rg_stage_valid[i-1];
            end
        endrule
        method Action start(Tuple3#(FloatingPoint#(11,52),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
            match {.in,.roundmode,.exc} = operands;
    //      `logLevel( tb, 0, $format("input in module %h", in))
            let lv_sign = pack(in.sign);
            Integer lv_m_val = 'd52;
            Integer lv_e_val = 'd11;
            Bit#(11) lv_exponent = in.exp;
            Bit#(52) lv_mantissa = in.sfd;
            let rounding_mode = roundmode;
            let flags = pack(exc);
            bit lv_overflow = 0;
            bit lv_zero = flags[3];
            bit lv_infinity = flags[1];
            bit lv_invalid = flags[0] | flags[2];
            bit lv_denormal = flags[4];
            bit lv_manzero = |lv_mantissa;
            bit lv_inexact = 0;
            Bool to_round = False; 
            Bool rne = (rounding_mode == Rnd_Nearest_Even);
            Bool rtz = (rounding_mode == Rnd_Zero);
            Bool rdn = (rounding_mode == Rnd_Minus_Inf);
            Bool rup = (rounding_mode == Rnd_Plus_Inf);
            Bool rmm = (rounding_mode == Rnd_Nearest_Away_Zero);
            
            let lv_e_val_1 = lv_e_val+1;
            Bit#(12) lv_exp = {1'b0,lv_exponent};
            Int#(11) lv_original_exponent;
            lv_original_exponent = unpack(truncate(lv_exp - 1023));  // removing the bias
            Bit#(`ELEN) final_result = 0;
            Bit#(TAdd#(52, `ELEN)) final_man = {'0,1'b1,lv_mantissa}; //parametrised for 'm' value
            if(lv_zero == 1)
                final_result = 0;
            else if(lv_denormal == 1 || (lv_original_exponent <= -1 && (lv_infinity|lv_invalid) == 0)) begin
                if(lv_sign==1 && convert_unsigned==1 && ((lv_original_exponent==-1 && (rmm||(rne && lv_manzero==1))) || (lv_original_exponent<=-1 &&rdn)))
                    lv_invalid = 1;
                else
                    lv_inexact = 1;
                if(lv_sign == 0 && rup)
                    final_result = 1;
                else if(rdn && lv_sign == 1 && convert_unsigned == 0)
                    final_result = '1;
                else if(lv_original_exponent == -1 && (rmm||(rne && lv_manzero == 1)))begin
                    if(lv_sign == 0)
                        final_result = 1;
                    else if(convert_unsigned == 0)
                        final_result = '1;
                    else
                        final_result = 0;
                end
                else
                    final_result = 0;
            end
            else if(convert_long == 0) 
            begin         //FCVT.W.S FCVT.WU.S or  //FCVT.W.D FCVT.WU.D
                if(convert_unsigned == 0) //FCVT.W.S or //FCVT.W.D
                begin 
                    Bit#(31) all_ones = '1;
                    if(lv_infinity == 1 || lv_invalid == 1) 
                    begin
                        final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(32'h80000000)) : zeroExtend(all_ones); 
                    end
                    else if(lv_original_exponent < 'd31) 
                    begin
                        final_man = final_man << lv_original_exponent;
                        Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                        final_result = signExtend(y);
                        lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value 
                        to_round = True;
                    end
                    else if(lv_original_exponent >= 'd31) 
                    begin
                        lv_invalid = 1;
                        if(lv_sign == 0)
                            final_result = zeroExtend(all_ones);
                        else 
                        begin
                            if(lv_original_exponent == 'd31 && lv_manzero == 0)
                                lv_invalid = 0 ;        //Since we are exactly representing the number? 
                            final_result = signExtend(32'h80000000);
                        end
                    end
                end
                else begin     //FCVT.WU.S or //FCVT.WU.D
                    Bit#(32) all_ones = '1;
                    if(lv_infinity == 1 || lv_invalid == 1)
                        final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
                    else if(lv_original_exponent < 'd32) begin
                        final_man = final_man << lv_original_exponent;
                        Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                        final_result = signExtend(y);
                        lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value
                        to_round = True;
                    end
                    else if(lv_original_exponent >= 'd32) begin
                        lv_invalid = 1;
                        if(lv_sign == 0)
                            final_result = signExtend(all_ones);
                        else
                            final_result = '0;
                    end
                end
            end
            `ifdef RV64
                else begin
                    if(convert_unsigned == 0) begin //FCVT.L.S or //FCVT.L.D
                        Bit#(63) all_ones = '1;
                        if(lv_infinity == 1 || lv_invalid == 1)
                            final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(64'h8000000000000000)) : zeroExtend(all_ones); 
                        else if(lv_original_exponent < 'd63) begin
                            final_man = final_man << lv_original_exponent;
                            Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                            final_result = zeroExtend(y);
                            lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                            to_round = True;
                        end
                        else if(lv_original_exponent >= 'd63) begin
                            lv_invalid = 1;
                            if(lv_sign == 0)
                                final_result = zeroExtend(all_ones);
                            else begin
                                if(lv_original_exponent == 'd63 && lv_manzero == 0 )
                                    lv_invalid = 0;  //Since we are exactly representing the input number
                                final_result = signExtend(64'h8000000000000000);
                            end
                        end 
                    end
                    else begin     //FCVT.LU.S or //FCVT.LU.D
                        Bit#(64) all_ones = '1;
                        if(lv_infinity == 1 || lv_invalid == 1)
                            final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
                        else if(lv_original_exponent < 'd64) begin
                            final_man = final_man << lv_original_exponent;
                            Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                            final_result = zeroExtend(y);
                            lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                            to_round = True;
                        end
                        else if(lv_original_exponent >= 'd64) begin
                            lv_invalid = 1;
                            if(lv_sign == 0)
                                final_result = signExtend(all_ones);
                            else
                                final_result = '0;
                        end
                    end
                end
            `endif 
            bit lv_guard = lv_mantissa[lv_m_val-1];	        //MSB of the already shifted mantissa is guard bit //parametrised for 'm' value
            bit lv_round = lv_mantissa[lv_m_val-2];	        //next bit is round bit //parametrised for 'm' value
            bit lv_sticky = |(lv_mantissa<<2);		//remaining bits determine the sticky bit
            bit lv_round_up = 0;
            bit lv_inexact1 = lv_guard | lv_round | lv_sticky;
            if(to_round) begin
                if(rounding_mode == Rnd_Nearest_Even) 		
                    lv_round_up = lv_guard & (final_result[0] | lv_round | lv_sticky);	//Round to nearest ties to even
                else if(rmm) 
                    lv_round_up = lv_guard; //& (lv_round | lv_sticky | ~lv_sign);			//Round to nearest ties to max magnitude
                else if(rdn) 
                    lv_round_up = lv_inexact1 & (lv_sign);								//Round down to -infinity
                else if(rup) 
                    lv_round_up = lv_inexact1 & (~lv_sign);								//Round up to +infinity
                lv_inexact = lv_inexact | lv_inexact1;
                
                if(lv_round_up == 1) begin //Should set the overflow flag here right? 
                    lv_invalid = 1;
                    if(convert_long == 0 && convert_unsigned == 0 && lv_original_exponent == 30 && 
                        final_result[30:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
                        final_result = 64'h7fffffff; 

                    else if(convert_long == 0 && convert_unsigned == 1 && lv_original_exponent == 31 && final_result[31:0] == '1 && lv_sign == 0)
                        final_result = 64'hffffffffffffffff; //Should verify again

                    `ifdef RV64
                        else if(convert_long == 1 && convert_unsigned == 0 && lv_original_exponent == 62 && final_result[62:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
                            final_result = 64'h7fffffffffffffff;
                        else if(convert_long == 1 && convert_unsigned == 1 && lv_original_exponent == 63 && final_result[63:0] == '1 && lv_sign == 0)
                            final_result = 64'hffffffffffffffff;                
                    `endif

                    else begin
                        lv_invalid = 0;
                        final_result = final_result + 1;
                        if(convert_long == 0 && final_result[31]==1)
                        final_result = signExtend(final_result[31:0]);
                    end
                end
                if(convert_unsigned == 0 && lv_sign == 1)begin		//Negating the output if floating point number is negative and converted to signed word/long
                    final_result = ~final_result + 1;
                    if(convert_long == 0 && final_result[31] == 1)
                        final_result = signExtend(final_result[31:0]);
                end
                else if(convert_unsigned == 1 && lv_sign == 1) begin
                    final_result = 0;
                    lv_invalid = 1;
                end
            end
            if((lv_invalid|lv_infinity) == 1) begin  //What about Quiet NaN?? What does the Spec Say?
                lv_overflow = 0;
                lv_inexact = 0;
            end
            Bit#(5) fflags={lv_invalid|lv_infinity,1'b0,lv_overflow,1'b0,lv_inexact};
            rg_stage_out[0] <= Floating_output{
                                final_result: final_result,
                                fflags: fflags};
            rg_stage_valid[0] <= 1;
        endmethod
        //
        method ReturnTypeInt#(`ELEN) receive();
            return ReturnTypeInt{valid:rg_stage_valid[`STAGES_FCONV_FP_TO_INT-1],value:rg_stage_out[`STAGES_FCONV_FP_TO_INT-1].final_result,ex:unpack(rg_stage_out[`STAGES_FCONV_FP_TO_INT-1].fflags)};
        endmethod 
    endmodule
`endif // fpu_hierarchical

endpackage

