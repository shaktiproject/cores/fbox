////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
/* 
see LICENSE.iitm
--------------------------------------------------------------------------------------------------
Author: Shalender Kumar, Charulatha Narashiman, Mouna Krishna
Email id: cs18m050@smail.iitm.ac.in, charuswathi112@gmail.com, mounakrishna2398@gmail.com
--------------------------------------------------------------------------------------------------  
*/
`include "Logger.bsv"
package normalize_div_hierarchical;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

//`ifdef fpu_hierarchical 

function Tuple3#(FloatingPoint#(8, 23),Bit#(2),Exception) fn_normalize_sp(FloatingPoint#(8, 23) din, Bit#(29) sfdin, Bit#(5) zeros);

   FloatingPoint#(8, 23) out = din;
   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   Int#(TAdd#(8, 1)) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   //let zeros = countZerosMSB(sfdin);
   if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) begin
      out.exp = maxBound - 1;
      out.sfd = maxBound;
      guard = '1;
      exc.overflow = True;
      exc.inexact = True;
   end
   else begin
      if (zeros == 0) begin
	      // carry, no sfd adjust necessary
         if (out.exp == 0)
	         out.exp = 2;
         else
            out.exp = out.exp + 1;
         // carry bit
         sfdin = sfdin << 1;
      end
      else if (zeros == 1) begin
      // already normalized
         if (out.exp == 0)
            out.exp = 1;

      // carry, hidden bits
         sfdin = sfdin << 2;
      end
      else if (zeros == fromInteger(valueOf(29))) begin
      // exactly zero
         out.exp = 0;
      end
      else begin
      // try to normalize
         Int#(TAdd#(8, 1)) shift = zeroExtend(unpack(pack(zeros - 1)));
         Int#(TAdd#(8, 1)) maxshift = exp - fromInteger(minexp(out));

      `ifdef denormal_support
         if (shift > maxshift) begin
         // result will be subnormal
            sfdin = sfdin << maxshift;
            out.exp = 0;
         end
         else begin
         // result will be normal
            sfdin = sfdin << shift;
            out.exp = out.exp - truncate(pack(shift));
         end
         // carry, hidden bits
         sfdin = sfdin << 2;
      end
      `else
         if (shift <= maxshift) begin
         // result will be normal
            sfdin = sfdin << shift;
            out.exp = out.exp - truncate(pack(shift));
         end
	      sfdin = sfdin << 2;
         // carry, hidden bits
      end
      `endif
      
      out.sfd = unpack(truncateLSB(sfdin));
      guard[1] = sfdin[5];
      guard[0] = |sfdin[4:0];
   end

   if ((out.exp == 0) && (guard != 0))
      exc.underflow = True;

   return tuple3(out,guard,exc);
endfunction

function Tuple3#(FloatingPoint#(11, 52),Bit#(2),Exception) fn_normalize_dp(FloatingPoint#(11, 52) din, Bit#(58) sfdin, Bit#(11) zeros);

   FloatingPoint#(11, 52) out = din;
   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   Int#(TAdd#(11, 1)) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   //let zeros = countZerosMSB(sfdin);
   if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) begin
      out.exp = maxBound - 1;
      out.sfd = maxBound;
      guard = '1;
      exc.overflow = True;
      exc.inexact = True;
   end
   else begin
      if (zeros == 0) begin
      // carry, no sfd adjust necessary
         if (out.exp == 0)
            out.exp = 2;
         else
            out.exp = out.exp + 1;
      // carry bit
         sfdin = sfdin << 1;
      end
      else if (zeros == 1) begin
      // already normalized
         if (out.exp == 0)
         out.exp = 1;
      // carry, hidden bits
         sfdin = sfdin << 2;
      end
      else if (zeros == fromInteger(valueOf(58))) begin
      // exactly zero
         out.exp = 0;
      end
      else begin
         // try to normalize
         Int#(TAdd#(11, 1)) shift = zeroExtend(unpack(pack(zeros - 1)));
         Int#(TAdd#(11, 1)) maxshift = exp - fromInteger(minexp(out));

      `ifdef denormal_support
         if (shift > maxshift) begin
         // result will be subnormal
            sfdin = sfdin << maxshift;
            out.exp = 0;
         end
         else begin
         // result will be normal
            sfdin = sfdin << shift;
            out.exp = out.exp - truncate(pack(shift));
         end
         // carry, hidden bits
         sfdin = sfdin << 2;
      end
      `else
         if (shift <= maxshift) begin
         // result will be normal
         sfdin = sfdin << shift;
         out.exp = out.exp - truncate(pack(shift));
         end
      sfdin = sfdin << 2;
 	 // carry, hidden bits	
      end
      `endif
      
      out.sfd = unpack(truncateLSB(sfdin));
      guard[1] = sfdin[5];
      guard[0] = |sfdin[4:0];
   end

   if ((out.exp == 0) && (guard != 0))
      exc.underflow = True;

   return tuple3(out,guard,exc);
endfunction

//`endif

endpackage

