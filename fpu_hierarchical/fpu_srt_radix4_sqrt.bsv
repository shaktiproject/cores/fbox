////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Mouna Krishna
Email id: mounakrishna2398@gmail.com
--------------------------------------------------------------------------------------------------

This implementation is done referring the papers "Radix-4 square root without initial PLA", "Design issues in radix-4 SRT square root & divide unit" and the book "Digital Arithmetic".
References:
1. M. D. Ercegovac and T. Lang, "Radix-4 square rot without initial PLA," in IEEE Transactions on Computers, vol. 39, no. 8, pp. 1016-1024, Aug. 1990, doi: 10.1109/12.57040.
2. N. Burgess and C. Hinds, "Design issues in radix-4 SRT square root & divide unit," Conference Record of Thirty-Fifth Asilomar Conference on Signals, Systems and Computers (Cat.No.01CH37256), 2001, pp. 1646-1650 vol.2, doi: 10.1109/ACSSC.2001.987764.
3. Digital Arithmetic. Morgan Kaufmann Publishers 2004.

Sqrt_sp latency - 19 cycles.
Sqrt_dp latency - 34 cycles.
*/
////////////////////////////////////////////////////////////////////////////////
//  Filename      : fpu_srt_radix4_sqrt.bsv
////////////////////////////////////////////////////////////////////////////////
package fpu_srt_radix4_sqrt;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg              ::*;
  `include "Logger.bsv"

//`ifdef fpu_hierarchical

////////////////////////////////////////////////////////////////////////////////
/// Square Root
////////////////////////////////////////////////////////////////////////////////

interface Ifc_sqrt_sp;
   method Action send(Tuple2#(FloatingPoint#(8,23), RoundMode) operands);
   method ReturnType#(8,23) receive(); 
endinterface
	
interface Ifc_sqrt_dp;
   method Action send(Tuple2#(FloatingPoint#(11,52), RoundMode) operands);
   method ReturnType#(11,52) receive(); 
endinterface

// Sqrt SP
(*synthesize*)
module mk_sqrt_sp(Ifc_sqrt_sp);
   /*reg:doc: Stores the output for not exact conditions.*/
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(8,23),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*reg:doc: Stores the initial operands from the send method*/
   Reg#(Tuple2#(FloatingPoint#(8,23), RoundMode)) rg_operands <- mkReg(tuple2(unpack(0),unpack(0)));
   /*doc:reg: To store the residual in carry save form*/
   Reg#(Bit#(30)) rg_residual <- mkReg(0);
   Reg#(Bit#(30)) rg_residual_cry <- mkReg(0);
   /*doc:reg: To store the A[j] used for on-the-fly conversion and also ~A[j] */
   Reg#(Bit#(31)) rg_a <- mkReg(0);
   Reg#(Bit#(31)) rg_not_a <- mkReg(0);   //it's rg_a inverted but has trailing zeros.
   /*doc:reg: To store the B[j] used for on-the-fly conversion and also ~B[j]*/
   Reg#(Bit#(31)) rg_b <- mkReg(0);
   Reg#(Bit#(31)) rg_not_b <- mkReg(0);   //it's rg_b inverted but it has trailing zeros.
   /*doc:reg: To store the moving 1 for simpler calculation of F, a and b.*/
   Reg#(Bit#(31)) rg_k <- mkReg(0);
   /*doc:reg: to control the rules.*/
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   /*doc:reg: To track the iterations*/
   Reg#(Bit#(TLog#(15))) rg_count <- mkReg(0);
   /*doc:reg: Stores the result after checking for flags*/
   Reg#(Maybe#(FloatingPoint#(8,23))) rg_final_result <- mkReg(tagged Invalid);
   Reg#(Bit#(29)) rg_final_sfd <- mkReg(0);
   /*doc:reg: Stores the flags*/
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);
   /*doc:reg:Stores the final output when inputs are valid.*/
   Reg#(FloatingPoint#(8,23)) rg_final_out <- mkReg(unpack(0));

   /*doc:function:Returns the next root digit. Computed by subtracting the residual from the selections constants and the root digit is selected based on the sign of the reults.*/
   function Tuple2#(Bit#(2), Bit#(1)) fn_digit_select_new(Bit#(3) root_select, Bit#(8) residual, Bit#(8) residual_cry);
      Int#(8) m0=?, m1=?, m2=?, m_1=?;
      case(root_select)
         'b000: begin m_1=-26; m0=-8; m1=8; m2=25;  end 
         'b001: begin m_1=-29; m0=-10; m1=8; m2=28; end 
         'b010: begin m_1=-32; m0=-12; m1=8; m2=30; end 
         'b011: begin m_1=-34; m0=-12; m1=12; m2=34;end
         'b100: begin m_1=-36; m0=-12; m1=12; m2=36;end
         'b101: begin m_1=-40; m0=-16; m1=12; m2=40;end
         'b110: begin m_1=-44; m0=-16; m1=15; m2=42;end
         'b111: begin m_1=-46; m0=-16; m1=15; m2=46;end
      endcase

      Bit#(9) final_m_1, final_m0, final_m1, final_m2;
      final_m_1 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m_1));
      final_m0 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m0));
      final_m1 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m1));
      final_m2 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m2));

      Bit#(4) carry = {final_m2[8], final_m1[8], final_m0[8], final_m_1[8]};
      Bit#(2) s;
      Bit#(1) s_sign;
      if (carry[3] == 0) begin s = 2; s_sign = 0; end
      else if (carry[2] == 0) begin s = 1; s_sign = 0; end
      else if (carry[1] == 0) begin s = 0; s_sign = 0; end
      else if (carry[0] == 0) begin s = 1; s_sign = 1; end
      else begin s = 2; s_sign = 1; end

      return tuple2(s, s_sign);
   endfunction
   //*************************************************** rules ********************************************************************************
   /*doc:rule: Calculate MSB zeros of input, update exponent if input is valid, estimate the flags.*/
   rule rl_initial1(rg_en == 1);
      rg_en <= 2;
      match {.op, .rmode} = rg_operands;
      Exception exc = defaultValue;
      Maybe#(FloatingPoint#(8,23)) result = tagged Invalid;
      let out = op;
      Bit#(28) opsfd = ?;
      Bit#(24) sfd = {getHiddenBit(op), op.sfd};
      if (isSNaN(op)) begin
         exc.invalid_op = True;
	      result = tagged Valid nanQuiet(op);
      end
      else if (isQNaN(op) || isZero(op) || (isInfinity(op) && (op.sign == False))) begin
         result = tagged Valid op;
      end
      else if (op.sign) begin
         exc.invalid_op = True;
         result = tagged Valid qnan();
      end
      else begin
         Bit#(TAdd#(8,2)) exp = isSubNormal(op) ? fromInteger(minexp(op)) : signExtend(unpack(unbias(out)));
      `ifdef sqrt_zeros_split_sp
         //compute MSB 0s of sfd
         Bit#(24) inp = sfd;
         Vector#(15, Bit#(1)) v1 = unpack(inp[23:9]);                        
         Bit#(5) result1=0;                                  
         Bool done1 = False;
         for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
            if ((v1[p1] == 0)&&(!done1))  
               result1 = result1+1 ;
            else
               done1 = True;
         end
         Bit#(5) z0 = (result1);
   
         Vector#(9, Bit#(1)) v2 = unpack(inp[8:0]);                        
         Bit#(5) result2=0;                                   
         Bool done2 = False;
         for( Integer p2 = 8; p2 >=0; p2 = p2 - 1) begin
            if ((v2[p2] == 0)&&(!done2))  
               result2 = result2+1 ;
            else
               done2 = True;
         end
         Bit#(5) z1 = result2;
         Bit#(5) zeros = (|inp[23:9]==1)?z0:((|inp[8:0]==1)?(z1+15):24);
      `else
         Bit#(5) zeros = pack(countZerosMSB(sfd));
      `endif
         //calculated MSB 0s of sfd
         sfd = sfd << zeros;
         exp = exp - zeroExtend(unpack(pack(zeros)));
         opsfd = {sfd, 4'b0000}; //mantissa + 6 extra bits for higher precision used in rounding.
         //adjusting mantissa when exp is odd.
         if ((exp & 1) == 0) begin
            opsfd = opsfd >> 1;
         end
         //shifting the exponent appropriately and calculating the final new exponent.
         out.exp = truncate(pack(exp >> 1) + fromInteger(bias(out)));
      end       
      if (result matches tagged Invalid) begin
         rg_residual <= {2'b11, opsfd};          //storing mantissa-1 in residual. Has 2 integer bits and rest are fractional part.
         rg_residual_cry <= 0;
         rg_a <= 31'h10000000;                  //setting rg_a to initial_root. Has 3 integer bits and rest are fractional part
         rg_not_a <= 31'h60000000;
         rg_b <= 31'h00000000;                  //setting rg_b to initial_root - 1. Has 3 integer bits and rest are fractinal part.
         rg_not_b <= 31'h70000000;
         rg_k <= 31'h04000000;
      end
      rg_final_result <= result;
      rg_final_out <= out;
      rg_final_exc <= exc;
   endrule

   /*doc:rule: Finds the next digit, updates the root using on the fly conversion, partial residual calculated in carry save form*/
   rule rl_loop(rg_en == 2 && rg_count <= fromInteger(valueOf(13)));
      rg_count <= rg_count + 1;
      Bit#(32) r = extend(rg_residual) << 2;
      Bit#(32) r_cry = extend(rg_residual_cry) << 2;
      let qp = rg_a;
      let qm = rg_b;
      let not_qp = rg_not_a;
      let not_qm = rg_not_b;

      //First iteration 101 is selected, other iterations if msb of rg_a is 1 then 111 is selcted else the usual bits (3 bits after the MSB of fractional part) are selected.
      Bit#(3) root_select = (rg_count!=0)? ((qp[28]==1)? 3'b111 : qp[26:24]) : 3'b101; 
      let {s, s_sign} = fn_digit_select_new(root_select, r[31:24], r_cry[31:24]);

      ////Digit update and residual calculation logic. Done based on design issues paper.
      //These constant values are used for calculation of other values f, a, b.
      Bit#(31) const_k = rg_k;
      //f is the part added to present residual to calculate the next residual. F = -2*root*s[j] - s^2/4^(j+1), where j is the iteration number.
      //This complex calculation is done in a simpler way as mentioned in "Design issues in radix-4 SRT square root & divide unit" paper.
      Bit#(31) f = ?;   
      //The new variables of A[j] and B[j] are stored in these variables.
      Bit#(31) new_qp = ?;
      Bit#(31) new_not_qp = ?;
      Bit#(31) new_qm = ?;
      Bit#(31) new_not_qm = ?;
      //Gets the new computed partial residual in carry save form.
      Bit#(30) r_new = ?;
      Bit#(30) r_cry_new = ?;

      //The updating of all the below values are done based on "Design issues in radix-4 SRT square root & divide unit" paper.
      if (s == 2) begin
         if (s_sign == 0) begin
            f = (not_qp << 2) | ((const_k | const_k << 1) << 2);
            new_qp = qp | (const_k << 1);           //appending '10' with qp
            new_not_qp = not_qp | const_k;
            new_qm = qp | const_k;
            new_not_qm = not_qp | (const_k << 1);   //appending '10' with ~Qp
         end
         else begin
            f = (qm << 2) | ((const_k | const_k << 1) << 2);
            new_qp = qm | (const_k << 1);           //appending '10' with Qm
            new_not_qp = not_qm | const_k;
            new_qm = qm | const_k;
            new_not_qm = not_qm | (const_k << 1);   //appending '10' with ~Qm
         end
      end
      else if (s == 1) begin
         if (s_sign == 0) begin
            f = (not_qp << 1) | const_k | (const_k << 1) | (const_k << 2);
            new_qp = qp | const_k;            //appending '01' with Qp
            new_not_qp = not_qp | (const_k << 1);
            new_qm = qp;
            new_not_qm = not_qp | (const_k << 1) | const_k;  //appending '11' with ~Qp
         end
         else begin
            f = (qm << 1) | const_k | (const_k << 1) | (const_k << 2);
            new_qp = qm | (const_k << 1) | const_k;          //appending '11' with Qm
            new_not_qp = not_qm;
            new_qm = qm | (const_k << 1);
            new_not_qm = not_qm | const_k;    //appending '01' with ~Qm
         end
      end
      else begin
         f = 0;
         new_qp = qp;                        //the same Qp
         new_not_qp = not_qp | const_k | (const_k << 1);
         new_qm = qm | const_k | (const_k << 1);
         new_not_qm = not_qm;                //the same ~Qm
      end
      
      //updating residual in carry save form - A parallel Full Adder, add of all bits seperately, the partial residual and carry calculated is stored seperately.
      r_cry_new[0] = 0;
      for (Integer i=0; i<29; i=i+1) begin
         Bit#(2) temp = zeroExtend(r[i]) + zeroExtend(f[i]) + zeroExtend(r_cry[i]);
         r_new[i] = temp[0];
         r_cry_new[i+1] = temp[1];
      end
      r_new[29] = r[29] + f[29] + r_cry[29];
      
      //Updating all the registers with the new values.
      rg_residual <= r_new;
      rg_residual_cry <= r_cry_new;
      rg_a <= new_qp;
      rg_not_a <= new_not_qp;
      rg_b <= new_qm;
      rg_not_b <= new_not_qm;
      rg_k <= const_k>>2;
      //$display("[%d] a is %h, b is %h, residual is %h, cry is %h, F is %h, s is %d with sign %d", rg_count, new_qp, new_qm, r_new, r_cry_new, f, s, s_sign);
   endrule: rl_loop

   /*doc:rule: Terminal step - updating root according to residual sign and whether it is zero.
   Post normalisation and rounding.*/
   rule rl_final1(rg_en1 == 1 && rg_count > fromInteger(valueOf(13)));
      rg_en1 <= 2;
      Bit#(TAdd#(28,1)) sfd1 = ?;
      let result = rg_final_result;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let a = rg_a;
      let residual = rg_residual;
      let residual_cry = rg_residual_cry;
      Bool rem_zero = (residual + residual_cry == 0);
      Bool rem_sign = unpack((residual + residual_cry)[28]);
      //Termination steps based on residual after loop.
      if (result matches tagged Invalid) begin
         if (rem_sign) begin  //Decreements root by 1 when residual is negative.
            a = a - 1;
         end
         if (!rem_zero) a[0] = 1;  //Makes LSB of a as 1 if residual is 0.
         sfd1 = truncate(a);
      end
      rg_final_result <= result;
      rg_final_sfd <= sfd1;
      rg_final_out <= out;
   endrule:rl_final1

   /*doc:rule:Calculating the guard bits and rounding*/
   rule rl_final2(rg_en1 == 2);
      rg_en1 <= 0;
      rg_en <= 0;
      let result = rg_final_result;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let sfd1 = rg_final_sfd<<2;
      let rmode = tpl_2(rg_operands);
      Bit#(2) guard = ?;
      if (result matches tagged Invalid) begin
         out.sfd = truncateLSB(sfd1);
         guard[1] = sfd1[5];
         guard[0] = |sfd1[4:0];
      end

      if (result matches tagged Valid .x) begin
         out = x;
      end
      else begin
         match {.out_, .exc_} = round(rmode, out, guard);
         out = out_;
         exc = exc | exc_;
      end
      rg_result <= tuple3(1'b1, canonicalize(out), exc);
   endrule
   //******************************************************************************************************************************************
   method Action send(Tuple2#(FloatingPoint#(8,23), RoundMode) operands);
      rg_en <= 1;
      rg_count <= 0;
      rg_en1 <= 1;
      rg_operands <= operands;
   endmethod
	
   method ReturnType#(8,23) receive();
      let y = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return y;
   endmethod
endmodule

// Sqrt DP
(*synthesize*)
module mk_sqrt_dp(Ifc_sqrt_dp);
   /*reg:doc: Stores the output for not exact conditions.*/
   Reg#(Tuple3#(Bit#(1),FloatingPoint#(11,52),Exception)) rg_result <- mkDReg(tuple3(unpack(0),unpack(0),unpack(0)));
   /*reg:doc: Stores the initial operands from the send method*/
   Reg#(Tuple2#(FloatingPoint#(11,52), RoundMode)) rg_operands <- mkReg(tuple2(unpack(0),unpack(0)));
   /*doc:reg: To store the residual in carry save form*/
   Reg#(Bit#(58)) rg_residual <- mkReg(0);
   Reg#(Bit#(58)) rg_residual_cry <- mkReg(0);
   /*doc:reg: To store the A[j] used for on-the-fly conversion and also ~A[j] */
   Reg#(Bit#(59)) rg_a <- mkReg(0);
   Reg#(Bit#(59)) rg_not_a <- mkReg(0);   //it's rg_a inverted but it has trailing zeros.
   /*doc:reg: To store the B[j] used for on-the-fly conversion and also ~B[j]*/
   Reg#(Bit#(59)) rg_b <- mkReg(0);
   Reg#(Bit#(59)) rg_not_b <- mkReg(0);   //it's rg_b inverted but it has trailing zeros.
   /*doc:reg: To store the moving 1 used for a and b calculation*/
   Reg#(Bit#(59)) rg_k <- mkReg(0);
   /*doc:reg: to control the rules.*/
   Reg#(Bit#(2)) rg_en <- mkReg(0);
   Reg#(Bit#(2)) rg_en1 <- mkReg(0);
   Reg#(Bool) rg_en2 <- mkReg(False);
   /*doc:reg: To track the iterations*/
   Reg#(Bit#(6)) rg_count <- mkReg(0);
   /*doc:reg: Stores the result after checking for flags*/
   Reg#(Maybe#(FloatingPoint#(11,52))) rg_final_result <- mkReg(tagged Invalid);
   /*doc:reg:To store mantissa calculated in rl_final1 to be used in rl_final2.*/
   Reg#(Bit#(56)) rg_final_sfd <- mkReg(0);
   /*doc:reg:To store input's mantissa calculated in rl_initial0 to be used in rl_initial1.*/
   Reg#(Bit#(53)) rg_sfd <- mkReg(0);
   /*doc:reg:Stores the exponent subtracted by MSB zeros count. Used in rl_initial*/
   Reg#(Bit#(13)) rg_newexp <- mkReg(0);
   /*doc:reg:Stores the final output when inputs are valid.*/
   Reg#(FloatingPoint#(11,52)) rg_final_out <- mkReg(unpack(0));
   /*doc:reg:Stores the flags.*/
   Reg#(Exception) rg_final_exc <- mkReg(defaultValue);

   /*doc:function:Returns the next root digit. Computed by subtracting the residual from the selections constants and the root digit is selected based on the sign of the reults.*/
   function Tuple2#(Bit#(2), Bit#(1)) fn_digit_select_new(Bit#(3) root_select, Bit#(8) residual, Bit#(8) residual_cry);
      Int#(8) m0=?, m1=?, m2=?, m_1=?;
      
      case(root_select)
         'b000: begin m_1=-26; m0=-8; m1=8; m2=25;  end 
         'b001: begin m_1=-29; m0=-10; m1=8; m2=28; end 
         'b010: begin m_1=-32; m0=-12; m1=8; m2=30; end 
         'b011: begin m_1=-34; m0=-12; m1=12; m2=34;end
         'b100: begin m_1=-36; m0=-12; m1=12; m2=36;end
         'b101: begin m_1=-40; m0=-16; m1=12; m2=40;end
         'b110: begin m_1=-44; m0=-16; m1=15; m2=42;end
         'b111: begin m_1=-46; m0=-16; m1=15; m2=46;end
      endcase
      
      Bit#(9) final_m_1, final_m0, final_m1, final_m2;

      final_m_1 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m_1));
      final_m0 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m0));
      final_m1 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m1));
      final_m2 = pack(signExtend(unpack(residual + residual_cry)) - signExtend(m2));

      Bit#(2) s;
      Bit#(1) s_sign;
      if (final_m2[8] == 0) begin s = 2; s_sign = 0; end
      else if (final_m1[8] == 0) begin s = 1; s_sign = 0; end
      else if (final_m0[8] == 0) begin s = 0; s_sign = 0; end
      else if (final_m_1[8] == 0) begin s = 1; s_sign = 1; end
      else begin s = 2; s_sign = 1; end

      return tuple2(s, s_sign);
   endfunction
   //*************************************************** rules ********************************************************************************
   /*doc:rule:Calculates the MSB zeros in the input. Calculated in two ways - manual calculation and by using built in function. Anything can be used.*/
   rule rl_initial0 (rg_en == 1);
      rg_en <= 2;
      match {.op, .rmode} = rg_operands;
      let out = op;
      Bit#(53) sfd = {getHiddenBit(op), op.sfd};
      Bit#(TAdd#(11,2)) exp = isSubNormal(op) ? fromInteger(minexp(op)) : signExtend(unpack(unbias(out)));
      // compute MSB 0s of sfd
   `ifdef sqrt_zeros_split_dp
      Bit#(53) inp = sfd;
      Vector#(15, Bit#(1)) v1 = unpack(inp[52:38]);                        
      Bit#(6) result1=0;                                  
      Bool done1 = False;
      for( Integer p1 = 14; p1 >=0; p1 = p1 - 1) begin
         if ((v1[p1] == 0)&&(!done1))  
            result1 = result1+1 ;
         else
            done1 = True;
      end
      Bit#(6) z0 = (result1);
   
      Vector#(15, Bit#(1)) v2 = unpack(inp[37:23]);                       
      Bit#(6) result2=0;                                   
      Bool done2 = False;
      for( Integer p2 = 14; p2 >=0; p2 = p2 - 1) begin
         if ((v2[p2] == 0)&&(!done2))  
            result2 = result2+1 ;
         else
           done2 = True;
      end
      Bit#(6) z1 = result2;
   
      Vector#(15, Bit#(1)) v3 = unpack(inp[22:8]);                        
      Bit#(6) result3=0;                                   
      Bool done3 = False;
      for( Integer p3 = 14; p3 >=0; p3 = p3 - 1) begin
         if ((v3[p3] == 0)&&(!done3))  
            result3 = result3+1 ;
         else
            done3 = True;
      end
      Bit#(6) z2 = result3;

      Vector#(8, Bit#(1)) v4 = unpack(inp[7:0]);                        
      Bit#(6)  result4=0;                                   
      Bool done4 = False;
      for( Integer p4 = 7; p4 >=0; p4 = p4 - 1) begin
         if ((v4[p4] == 0)&&(!done4))  
            result4 = result4+1 ;
         else
            done4 = True;
      end
      Bit#(6)  z3 = result4;
      Bit#(6) zeros= (|inp[52:38]==1)?z0:((|inp[37:23]==1)?(z1+15):((|inp[22:8]==1)?(z2+30):((|inp[7:0]==1)?(z3+45):53)));
   `else
      Bit#(6) zeros = pack(countZerosMSB(sfd));
   `endif

      //shifting the mantissa appropriately and calculating the final new exponent.
      sfd = sfd << zeros;
      exp = exp - zeroExtend(unpack(pack(zeros)));

      rg_sfd <= sfd;
      rg_newexp <= exp;
   endrule:rl_initial0

   /*doc:rule:Estimate the flags and initialising the appropriate registers.*/
   rule rl_initial1(rg_en == 2);
      rg_en <= 3;
      match {.op, .rmode} = rg_operands;
      Exception exc = defaultValue;
      Maybe#(FloatingPoint#(11,52)) result = tagged Invalid;
      let out = op;
      Bit#(56) opsfd = ?;
      Bit#(53) sfd = rg_sfd;
      let exp = rg_newexp;
      
      if (isSNaN(op)) begin
         exc.invalid_op = True;
         result = tagged Valid nanQuiet(op);
      end
      else if (isQNaN(op) || isZero(op) || (isInfinity(op) && (op.sign == False))) begin
         result = tagged Valid op;
      end
      else if (op.sign) begin
         exc.invalid_op = True;
         result = tagged Valid qnan();
      end
      else begin
         opsfd = {sfd, 3'b000}; //mantissa + 5 extra bits for higher precision used in rounding.
         if ((exp & 1) == 0) begin
            opsfd = opsfd >> 1;
         end
         out.exp = truncate(pack(exp >> 1) + fromInteger(bias(out)));
      end
      
      rg_residual <= {2'b11, opsfd};
      rg_residual_cry <= 0;
      rg_a <= 59'h100000000000000;                  //setting rg_a to initial_root which is s[0] = 1.
      rg_not_a <= 59'h600000000000000;
      rg_b <= 59'h000000000000000;                  //setting rg_b to initial_root - 1.
      rg_not_b <= 59'h700000000000000;
      rg_k <= 59'h040000000000000;
      rg_final_result <= result;
      rg_final_out <= out;
      rg_final_exc <= exc;

   endrule:rl_initial1
 
   /*doc:rule: Finds the next digit, updates the root using on the fly conversion, partial residual calculated in carry save form*/
   rule rl_loop(rg_en == 3 && rg_count <= fromInteger(valueOf(27)));
      rg_count <= rg_count + 1;
      Bit#(60) r = extend(rg_residual) << 2;
      Bit#(60) r_cry = extend(rg_residual_cry) << 2;
      let qp = rg_a;
      let qm = rg_b;
      let not_qp = rg_not_a;
      let not_qm = rg_not_b;
      //First iteration 101 is selected, other iterations if msb of a is 1 then 111 is selcted else the usual bits are selected.
      Bit#(3) root_select = (rg_count!=0)? ((qp[56]==1)? 3'b111 : qp[54:52]) : 3'b101;

      //digit select logic 
      let {s, s_sign} = fn_digit_select_new(root_select, r[59:52], r_cry[59:52]);
      
      //digit update and residual calculation logic. Done based on design issued paper.
      Bit#(59) const_k = rg_k;
      //f is the part added to present residual to calculate the next residual. F = -2*root*s[j] - s^2/4^(j+1), where j is the iteration number.
      //This complex calculation is done in a simpler way as mentioned in "Design issues in radix-4 SRT square root & divide unit" paper.
      Bit#(59) f = ?;
      Bit#(59) new_qp = ?;
      Bit#(59) new_not_qp = ?;
      Bit#(59) new_qm = ?;
      Bit#(59) new_not_qm = ?;
      Bit#(58) r_new = ?;
      Bit#(58) r_cry_new = ?;

      //The updating of all the below values are done based on "Design issues in radix-4 SRT square root & divide unit" paper.
      if (s == 2) begin
         if (s_sign == 0) begin
            f = (not_qp << 2) | ((const_k | const_k << 1) << 2);
            new_qp = qp | (const_k << 1);           //appending '10' with qp
            new_not_qp = not_qp | const_k;
            new_qm = qp | const_k;
            new_not_qm = not_qp | (const_k << 1);   //appending '10' with ~Qp
         end
         else begin
            f = (qm << 2) | ((const_k | const_k << 1) << 2);
            new_qp = qm | (const_k << 1);           //appending '10' with Qm
            new_not_qp = not_qm | const_k;
            new_qm = qm | const_k;
            new_not_qm = not_qm | (const_k << 1);   //appending '10' with ~Qm
         end
      end
      else if (s == 1) begin
         if (s_sign == 0) begin
            f = (not_qp << 1) | const_k | (const_k << 1) | (const_k << 2);
            new_qp = qp | const_k;            //appending '01' with Qp
            new_not_qp = not_qp | (const_k << 1);
            new_qm = qp;
            new_not_qm = not_qp | (const_k << 1) | const_k;  //appending '11' with ~Qp
         end
         else begin
            f = (qm << 1) | const_k | (const_k << 1) | (const_k << 2);
            new_qp = qm | (const_k << 1) | const_k;          //appending '11' with Qm
            new_not_qp = not_qm;
            new_qm = qm | (const_k << 1);
            new_not_qm = not_qm | const_k;    //appending '01' with ~Qm
         end
      end
      else begin
         f = 0;
         new_qp = qp;                        //the same Qp
         new_not_qp = not_qp | const_k | (const_k << 1);
         new_qm = qm | const_k | (const_k << 1);
         new_not_qm = not_qm;                //the same ~Qm
      end
      
      //updating residual in carry save form - A parallel Full Adder, add of all bits seperately, the partial residual and carry calculated is stored seperately.
      r_cry_new[0] = 0;
      for (Integer i=0; i<57; i=i+1) begin
         Bit#(2) temp = zeroExtend(r[i]) + zeroExtend(f[i]) + zeroExtend(r_cry[i]);
         r_new[i] = temp[0];
         r_cry_new[i+1] = temp[1];
      end
      r_new[57] = r[57] + f[57] + r_cry[57];
      
      rg_residual <= r_new;
      rg_residual_cry <= r_cry_new;
      rg_a <= new_qp;
      rg_not_a <= new_not_qp;
      rg_b <= new_qm;
      rg_not_b <= new_not_qm;
      rg_k <= const_k>>2;
      //$display("[%d, %d, %d] a is %h, b is %h, residual is %h, s is %d with sign %d", rg_count, root_select, residual_int, new_qp, new_qm, r_new + r_cry_new, s, s_sign);
   endrule :rl_loop

   /*doc:rule: Terminal step - updating root according to residual sign and whether it is zero.*/
   rule rl_final1(rg_en1 == 1 && rg_count > fromInteger(valueOf(27)));
      rg_en1 <= 3;
      Bit#(56) sfd1 = ?;
      let result = rg_final_result;
      let out = rg_final_out;
      let exc = rg_final_exc;
      let a = rg_a;
      let residual = rg_residual;
      let residual_cry = rg_residual_cry;
      Bool rem_zero = (residual + residual_cry == 0);
      Bool rem_sign = unpack((residual + residual_cry)[56]);
      
      if (result matches tagged Invalid) begin
         if (rem_sign) a = a - 1;
         if (!rem_zero) a[0] = 1;
         sfd1 = truncate(a);
      end
      rg_final_result <= result;
      rg_final_sfd <= sfd1;
      rg_final_out <= out;
   endrule: rl_final1

   /*doc:rule:Calculating the guard bits and rounding*/
   rule rl_final2(rg_en1 == 3);
      rg_en1 <= 0;
      rg_en <= 0;
      Bit#(2) guard = ?;
      let result = rg_final_result;
      let out = rg_final_out;
      let rmode = tpl_2(rg_operands);
      let exc = rg_final_exc;
      let sfd1 = rg_final_sfd << 1;
      if (result matches tagged Invalid) begin
         out.sfd = truncateLSB(sfd1);
         guard[1] = sfd1[3];
         guard[0] = |sfd1[2:0];
      end
      if (result matches tagged Valid .x) begin
         out = x;
      end
      else begin
         match {.out_, .exc_} = round(rmode, out, guard);
         out = out_;
         exc = exc | exc_;
      end
      rg_result <= tuple3(1'b1, canonicalize(out), exc);
   endrule: rl_final2
   //******************************************************************************************************************************************
   
   method Action send(Tuple2#(FloatingPoint#(11,52), RoundMode) operands); 
      rg_en <= 1;
      rg_count <= 0;
      rg_en1 <= 1;
      rg_operands <= operands;
   endmethod
	
   method ReturnType#(11,52) receive(); 
      let x = ReturnType{valid:tpl_1(rg_result),value:tpl_2(rg_result),ex:tpl_3(rg_result)};
      return x;
   endmethod
endmodule

//`endif
endpackage
