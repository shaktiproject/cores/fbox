# CHANGELOG

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.5] - 2022-06-09
## Fixed
- Fpu_pipelined: Fixes for overflow and underflow tinyness mismtaches (sp & dp)

## [2.1.4] - 2022-03-15
## Fixed
- Fpu_pipelined: Valid bits for sp and dp fpu_adder

## [2.1.3] - 2022-03-14
## Added
- Fpu_pipelined: fpu_adder balancing stages, optimization (sp & dp)

## [2.1.2] - 2022-02-21
## Added
- Fpu_pipelined: Pipelined FMA sp and dp modules

## [2.1.1] - 2022-02-10
## Fixed
- Fpu_pipelined: Balancing/Optimization of dp-to-sp converter

## [2.1.0] - 2022-02-03
## Fixed
- Fpu_pipelined: Changes to fix the output and flag mismatches in fpu_booth_multiplier

## [2.0.9] - 2022-01-22
## Fixed
- Fpu_hierarchical/modules_fp: Fix tininess detection in fma sp and dp modules in fpu_fma_hierarchical

## [2.0.8] - 2021-11-17
## Fixed
- Fpu_pipelined: fpu_convert_pipelined: Rewrite sp_to_int and dp_to_int converters (balancing and optimization, 3 cycles)

## [2.0.7] - 2021-09-30
## Fixed
- Fpu_hierarchical: fpu_fma_hierarchical: Split the stage 3 of dp into two for timing; cleanup

## [2.0.6] - 2021-09-20
## Fixed
- Fpu_pipelined: fpu_convert_pipelined: Rewrite int_to_sp and int_to_dp converters (balancing and optimization, 3 cycles); cleanup

## [2.0.5] - 2021-09-17
## Fixed
- Fpu_pipelined: fpu_booth_multiplier: balanced and optimized stages in SP and DP multipliers; changed some functions in fpu_multiplier_common; cleanup

## [2.0.4] - 2021-08-31
## Added
- Fpu_hierarchical: SRT Radix4 divide and square root for SP and DP (4 separate modules)

## [2.0.3] - 2021-08-21
## Fixed
- Timing Optimizations: Added dummy stages to a few fp units

## [2.0.2] - 2021-07-15
## Fixed
- Single cycle fp modules: Changed single-cycle method from ActionValue to Value.

## [2.0.1] - 2021-06-27
## Added
- Fpu_pipelined: BSV testbenches added for all the modules

## [2.0.0] - 2021-06-18
## Fixed
- Fpu_div_sqrt_hierarchical: Changed BSV types in divide modules from Int to Bit

## [1.9.9] - 2021-06-11
## Fixed
- Fpu_pipelined: minor compile fix

## [1.9.8] - 2021-06-11
## Fixed
- Fpu_pipelined: Fpu_adder compile fix

## [1.9.7] - 2021-06-10
## Added
- Fpu_pipelined: Fpu_adder module added

## [1.9.6] - 2021-05-28
## Fixed
- Fpu_fma_hierarchical: fpu_div_sqrt_hierarchical, fpu_fma_hierarchical, normalize_fma_hierarchical cleanup

## [1.9.5] - 2021-04-27
## Fixed
- Fpu_fma_hierarchical: minor fix

## [1.9.4] - 2021-04-19
## Fixed
- Fpu_convert_hierarchical: Changed module names
- Fpu_fma_hierarchical: Moved the pipeline registers

## [1.9.3] - 2021-04-13
## Fixed
- Fpu_div_sqrt_hierarchical: Fixes in the for loops
- Fpu_fma_hierarchical: Commented out the old unused fn

## [1.9.2] - 2021-04-10
## Fixed
- Fpu_hierarchical & modules_fp:  Separation of parameterized and sp, dp versions, optimization for countzerosMSB fn

## [1.9.1] - 2021-04-09
## Added
- Int_to_fp: Fix rounding mode encoding (nearest_even)

## [1.9.0] - 2021-04-09
## Added
- Int_to_fp: Minor naming changes and latency update in fpu_parameters

## [1.8.9] - 2021-04-07
## Added
- Int_to_fp converter module Added and minor compile fix in fpu_multiplier and fpu_convert modules

## [1.8.8] - 2021-02-25
## Fixed
- Directory: License and Contributors

## [1.8.7] - 2021-02-23
## Fixed
- FP convert modules: latency changes, cleanup

## [1.8.6] - 2021-02-23
## Fixed
- FPU_Converter cleaned

## [1.8.5] - 2021-02-21
## Added
- FPU_Converter sp-to-dp two-cycle version added

## [1.8.4] - 2020-12-31
## Fixed
- FPU_Converter minor compile fixed

## [1.8.3] - 2020-12-20
## Fixed
- FPU_Converter modification for dp_to_sp module

## [1.8.2] - 2020-12-15
## Added
- FPU_Converter Module modification for latency reduction

## [1.8.1] - 2020-12-15
## Fixed
- Minor compile fix

## [1.8.0] - 2020-12-15
## Fixed
- Coding style changes to fpu multiplier

## [1.7.9] - 2020-10-14
## Fixed
- Dummy gitlab-ci re-order and dummy merge to test pipeline
- Imports fixed

## [1.7.8] - 2020-10-13
## Fixed
- FMUL fixed for "Valid" bit issues

## [1.7.7] - 2020-10-08
## Added
- FMUL modification for latency reduction

## [1.7.6] - 2020-09-29
## Fixed
- Flags in fpu_common for correct verilog (even with older bsc versions)

## [1.7.5] - 2020-07-30
## Fixed
- FMA sp fixed for very small result in rmin rounding mode.

## [1.7.4] - 2020-07-29
## Fixed
- Comments added in FMA.

## [1.7.3] - 2020-07-20
## Fixed
- FMA fixed for very small result in rmin rounding mode.

## [1.7.3] - 2020-07-20
## Fixed
- FMA correction.

## [1.7.5] - 2020-07-20
## Fixed
- FMA compilation error resolved.

## [1.7.2] - 2020-07-16
## Fixed
- FMA rewrite

## [1.7.1] - 2020-06-23
## Fixed
- Setting fmin/fmax flags correctly

## [1.7.0] - 2020-06-20
## Fixed
- fma operand ordering fixed

## [1.6.9] - 2020-06-19
## Fixed
- fma operand ordering

## [1.6.8] - 2020-06-18
## Fixed
- fp classify

## [1.6.7] - 2020-06-11
## Fixed
- fp compare 

## [1.6.6] - 2020-05-25
## Fixed
- Makefile

## [1.6.5] - 2020-05-15
## Fixed
- sp->dp and dp->sp bug fix (flags)

## [1.6.4] - 2020-05-12
## Fixed
- enum: rounding mode ordering

## [1.6.3] - 2020-04-29
## Added
- sign injection to gitlab ci

## [1.6.2] - 2020-04-29
## Fixed
- gitlab ci

## [1.6.1] - 2020-04-29
## Fixed
- fixes for max mag rounding mode and fp multiplier flag mismatches

## [1.6.0] - 2020-04-15
## Added
- hierarchical fpu

## [1.5.4] - 2020-04-06
## Fixed 
- parameters

## [1.5.3] - 2020-04-31
## Added 
- module name fix for int to sp/dp and ci fix

## [1.5.2] - 2020-03-25
## Added 
- Number of stages parameter to interface for multicycle fp modules (added to int_to_fp modules)

## [1.5.1] - 2020-03-04
## Fixed
- Minor changes (display)

## [1.5.0] - 2020-03-04
## Fixed
- Changes to fp divider for timing closure.

## [1.4.4] - 2020-02-19
## Fixed
- Changes to Makefile (open bsc) and ci (adding other fp modules)

## [1.4.3] - 2020-02-06
## Fixed
- fixed some bugs in fma modules (from bluespec lib)

## [1.4.2] - 2020-01-09
## Fixed
- fixed ci typos to generate sp/dp conversion modules correctly

## [1.4.1] - 2020-01-08
## Fixed
- fixed Makefile.inc to generate add/sub verilog. Need to change this to generate all verilog once fpu.bsv is done.
- moved intefer_divider.bsv to obsolete

## [1.4.0] - 2020-01-08
## Fixed
- naming convention changes for fpu_convert (file with multiple modules)
- removed dead code from fpu_div_sqrt (now contains modules only 1 fp_divider and 1 fp_sqrt)
- removed unnecessary actionvalue from fpu_sign_injection
- changes for canonicalizing NaN outputs from every module in modules_fp
- changes to output structure of all modules in modules_fp - has only one ready bit output now

## [1.3.0] - 2019-12-31
## Fixed 
- removed verif components 
- updated ci to release verilog
- moved single-cycle-shakti design to obsolete

## [1.2.0] - 2019-12-19
## Fixed 
- gitlab ci
- updated verif test bench environment
- bsv testbench and corresponding Makefile fixed for more scalability and testing.

## [1.1.2] - 2019-12-18
## Fixed 
- fixed naming convention for files with one module
- removed dead code in fpu_multiplier.bsv
- corresponding instance name change in tb_float.bsv

## [1.1.1] - 2019-12-17
## Added
- added and verified changes to tb_float.bsv to accomodate for adding valid bit to output for fp units

## [1.1.0] - 2019-12-16
## Added
- renamed floating_point.bsv to fpu_common.bsv and moved dead code to fpu_common_backup.bsv
- added support for FMV.W.X and FMV.X.W
- added valid bit to output for all units except div/sqrt.
- added fpu_sign_injection.bsv.
## Fixed
- fixed denormal support errors in fpu_fma.bsv
- fixed soft-float build for riscv. Imported specialize files from spike
- enabled denormal support by default for bsv_test_bench

## [1.0.3] - 2019-11-29
## Added
- basic synth support script for sp_add_sub
- basic bsv based test-bench to run softfloat generated tests.
## Removed
- removed redundant modulesFP folder
## Fixed
- cicd script has been fixed as well

## [1.0.2] - 2019-11-27
## Added
- interface changes for add_sub
- modules for divider and sqrt functions

## [1.0.1] - 2019-11-20
## Added
- verification setup

