////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/*
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, spECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
---------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
--------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
package fpu_fma;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

////////////////////////////////////////////////////////////////////////////////
/// Floating point fused multiple accumulate
////////////////////////////////////////////////////////////////////////////////
   // check operands, compute exponent for multiply
   function Tuple2#(FloatingPoint#(e,m),Exception) fn_fpu_fma(Tuple4#(
		 FloatingPoint#(e,m),
		 FloatingPoint#(e,m),
		 Maybe#(FloatingPoint#(e,m)),
		 RoundMode) operands)
		 provisos(
      Add#(e,2,ebits),
      Add#(m,1,mbits),
      Add#(m,5,m5bits),
      Add#(mbits,mbits,mmbits),
      // per request of bsc
      Add#(1, a__, mmbits),
      Add#(m, b__, mmbits),
      Add#(c__, TLog#(TAdd#(1, mmbits)), TAdd#(e, 1)),
      Add#(d__, TLog#(TAdd#(1, m5bits)), TAdd#(e, 1)),
      Add#(1, TAdd#(1, TAdd#(m, 3)), m5bits)
      );

      match { .opB, .opC, .mopA, .rmode } = operands;
      CommonState#(e,m) s = CommonState {
	 res: tagged Invalid,
	 exc: defaultValue,
	 rmode: rmode
	 };

      Bool acc = False;
      FloatingPoint#(e,m) opA = 0;

      if (mopA matches tagged Valid .opA_) begin
	 opA = opA_;
	 acc = True;
      end

      Int#(ebits) expB = isSubNormal(opB) ? fromInteger(minexp(opB)) : signExtend(unpack(unbias(opB)));
      Int#(ebits) expC = isSubNormal(opC) ? fromInteger(minexp(opC)) : signExtend(unpack(unbias(opC)));
      
/*      if (isSubNormal(opB))
	 opB.sfd = 0;
      if (isSubNormal(opC))
	 opC.sfd = 0;*/
      
      Bit#(mbits) sfdB = { getHiddenBit(opB), opB.sfd };
      Bit#(mbits) sfdC = { getHiddenBit(opC), opC.sfd };

      Bool sgnBC = opB.sign != opC.sign;
      Bool infBC = isInfinity(opB) || isInfinity(opC);
      Bool zeroBC = isZero(opB) || isZero(opC);
      Int#(ebits) expBC = expB + expC;

      if (isSNaN(opA)) begin
	 s.res = tagged Valid nanQuiet(opA);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opB)) begin
	 s.res = tagged Valid nanQuiet(opB);
	 s.exc.invalid_op = True;
      end
      else if (isSNaN(opC)) begin
	 s.res = tagged Valid nanQuiet(opC);
	 s.exc.invalid_op = True;
      end
      else if (isQNaN(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (isQNaN(opB)) begin
	 s.res = tagged Valid opB;
      end
      else if (isQNaN(opC)) begin
	 s.res = tagged Valid opC;
      end
      else if ((isInfinity(opB) && isZero(opC)) || (isZero(opB) && isInfinity(opC)) || (isInfinity(opA) && infBC && (opA.sign != sgnBC))) begin
	 // product of zero and infinity or addition of opposite sign infinity
	 s.res = tagged Valid qnan();
	 s.exc.invalid_op = True;
      end
      else if (isInfinity(opA)) begin
	 s.res = tagged Valid opA;
      end
      else if (infBC) begin
	 s.res = tagged Valid infinity(sgnBC);
      end
      else if (isZero(opA) && zeroBC && (opA.sign == sgnBC)) begin
	 s.res = tagged Valid opA;
      end


      let sfdBC = primMul(sfdB, sfdC);


   // passthrough stage for multiply register retiming
 

   // normalize multiplication result
  

      FloatingPoint#(e,m) bc = defaultValue;
      Bit#(2) guardBC = ?;

      if (s.res matches tagged Invalid) begin
	 if (expBC > fromInteger(maxexp(bc))) begin
	    bc.sign = sgnBC;
	    bc.exp = maxBound - 1;
	    bc.sfd = maxBound;
	    guardBC = '1;

	    s.exc.overflow = True;
	    s.exc.inexact = True;
	 end
	 else if (expBC < (fromInteger(minexp_subnormal(bc))-2)) begin
	    bc.sign = sgnBC;
	    bc.exp = 0;
	    bc.sfd = 0;
	    guardBC = 0;

	    if (|sfdBC == 1) begin
	       guardBC = 1;
	       s.exc.underflow = True;
	       s.exc.inexact = True;
	    end
	 end
	 else begin
	    let shift = fromInteger(minexp(bc)) - expBC;
	    if (shift > 0) begin
               //XXX XXX XXX Removed subnormal logic
          // subnormal
`ifdef denormal_support
	       Bit#(1) sfdlsb = |(sfdBC << (fromInteger(valueOf(mmbits)) - shift));

	       sfdBC = sfdBC >> shift;
          sfdBC[0] = sfdBC[0] | sfdlsb;
`else
               bc.sfd = 0;
               sfdBC = 0;
          //guardBC = 0;
`endif
          bc.exp = 0;
	    end
	    else begin
	       bc.exp = cExtend(expBC + fromInteger(bias(bc)));
	    end

	    bc.sign = sgnBC;
	    let y = normalize(bc, sfdBC);
	    bc = tpl_1(y);
	    guardBC = tpl_2(y);
	    s.exc = s.exc | tpl_3(y);
	 end
      end





     
      Int#(ebits) expA = isSubNormal(opA) ? fromInteger(minexp(opA)) : signExtend(unpack(unbias(opA)));
      expBC = isSubNormal(bc) ? fromInteger(minexp(bc)) : signExtend(unpack(unbias(bc)));
      `ifdef denormal_support
         opA.sfd = opA.sfd;
      `else
         if (isSubNormal(opA))
         opA.sfd = 0;
      `endif
        
      Bit#(m5bits) sfdA = {1'b0, getHiddenBit(opA), opA.sfd, 3'b0};
      sfdBC = {1'b0, getHiddenBit(bc), bc.sfd, guardBC, 1'b0};

      Bool sub = opA.sign != bc.sign;

      Int#(ebits) exp = ?;
      Int#(ebits) shift = ?;
      Bit#(m5bits) x = ?;
      Bit#(m5bits) y = ?;
      Bool sgn = ?;

      if ((!acc) || (expBC > expA) || ((expBC == expA) && (sfdBC > sfdA))) begin
	 exp = expBC;
	 shift = expBC - expA;
	 x = sfdBC;
    y = sfdA;
   sgn = bc.sign;
      end
      else begin
	 exp = expA;
	 shift = expA - expBC;
	 x = sfdA;
	 y = sfdBC;
	 sgn = opA.sign;
      end

   
      if (s.res matches tagged Invalid) begin
	 if (shift < fromInteger(valueOf(m5bits))) begin
	    Bit#(m5bits) guard;

	    guard = y << (fromInteger(valueOf(m5bits)) - shift);
	    y = y >> shift;
	    y[0] = y[0] | (|guard);
	 end
	 else if (|y == 1) begin
	    y = 1;
	 end
      end

     
      let sum = x + y;
      let diff = x - y;

    

      FloatingPoint#(e,m) out = defaultValue;
      Bit#(2) guard = 0;

      if (s.res matches tagged Invalid) begin
	 Bit#(m5bits) sfd;

	 sfd = sub ? diff : sum;

	 out.sign = sgn;
	 out.exp = cExtend(exp + fromInteger(bias(out)));

	 let y = normalize(out, sfd);
	 out = tpl_1(y);
	 guard = tpl_2(y);
	 s.exc = s.exc | tpl_3(y);
      end


      if (s.res matches tagged Valid .x) begin
	 out = x;
      end
      else begin
	 let y = round(s.rmode, out, guard);
	 out = tpl_1(y);
	 s.exc = s.exc | tpl_2(y);

	 // adjust sign for exact zero result
	 if (acc && isZero(out) && !s.exc.inexact && sub) begin
	    out.sign = (s.rmode == Rnd_Minus_Inf);
	 end
      end

   if(s.exc.overflow == True && s.rmode == Rnd_Nearest_Even) begin
	out.exp = '1;
	out.sfd = '0;
   end
/*   else if(s.exc.underflow == True) begin
	Bool sign = True;
	Bit#(e) expo = 'hff;
	Bit#(m) man = 'b00000000000000000000000;
	let result = FloatingPoint{sign : sign,exp: expo,sfd: man};
	return tuple2(result,s.exc);   	
   end*/
   return tuple2(canonicalize(out),s.exc);
   
   endfunction

// Module for retiming
`ifdef fpu_hierarchical
   interface Ifc_fpu_fma_sp;
      method Action send(Tuple4#(
         FloatingPoint#(8,23),
         FloatingPoint#(8,23),
         Maybe#(FloatingPoint#(8,23)),
         RoundMode) operands);
      method ReturnType#(8,23) receive();
   endinterface
   //
   interface Ifc_fpu_fma_dp;
      method Action send(Tuple4#(
         FloatingPoint#(11,52),
         FloatingPoint#(11,52),
         Maybe#(FloatingPoint#(11,52)),
         RoundMode) operands);
      method ReturnType#(11,52) receive();
   endinterface
`endif
   interface Ifc_fpu_fma#(numeric type e, numeric type m, numeric type nos);
      method Action send(Tuple4#(
         FloatingPoint#(e,m),
         FloatingPoint#(e,m),
         Maybe#(FloatingPoint#(e,m)),
         RoundMode) operands);
//      method Tuple2#(Bit#(1),Tuple2#(FloatingPoint#(e,m),Exception)) receive();
      method ReturnType#(e,m) receive();
   endinterface
   module mk_fpu_fma(Ifc_fpu_fma#(e,m,nos))
      provisos(
         Add#(a__, TLog#(TAdd#(1, TAdd#(TAdd#(m, 1), TAdd#(m, 1)))), TAdd#(e, 1)),
         Add#(b__, TLog#(TAdd#(1, TAdd#(m, 5))), TAdd#(e, 1))
      );

      //Integer number_of_stages = 4;  
      Vector#(nos,Reg#(Tuple2#(FloatingPoint#(e,m),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(nos,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= valueOf(nos) -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
      method Action send(Tuple4#(
            FloatingPoint#(e,m),
            FloatingPoint#(e,m),
            Maybe#(FloatingPoint#(e,m)),
            RoundMode) operands);		
               rg_stage_out[0] <= fn_fpu_fma(operands);
               rg_stage_valid[0] <= 1;

      endmethod
//      method Tuple2#(Bit#(1),Tuple2#(FloatingPoint#(e,m),Exception)) receive();
      method ReturnType#(e,m) receive();
return ReturnType{valid:rg_stage_valid[valueOf(nos)-1],value:tpl_1(rg_stage_out[valueOf(nos)-1]) ,ex:tpl_2(rg_stage_out[valueOf(nos)-1])};
//         return tuple2(rg_stage_valid[`number_of_stages-1],rg_stage_out[`number_of_stages-1]);
      endmethod 
   endmodule

   // (*synthesize*)
   module mk_fpu_fma_sp_instance(Ifc_fpu_fma#(8,23,`STAGES_FMA_SP));
      let ifc();
      mk_fpu_fma _temp(ifc);
      return (ifc);
   endmodule
   // (*synthesize*)
   module mk_fpu_fma_dp_instance(Ifc_fpu_fma#(11,52,`STAGES_FMA_DP));
      let ifc();
      mk_fpu_fma _temp(ifc);
      return (ifc);
   endmodule

`ifdef fpu_hierarchical
   (*synthesize*)
   module mk_fpu_fma_sp(Ifc_fpu_fma_sp);
      Vector#(`STAGES_FMA_SP,Reg#(Tuple2#(FloatingPoint#(8,23),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(`STAGES_FMA_SP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= `STAGES_FMA_SP -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
      method Action send(Tuple4#(
            FloatingPoint#(8,23),
            FloatingPoint#(8,23),
            Maybe#(FloatingPoint#(8,23)),
            RoundMode) operands);		
               rg_stage_out[0] <= fn_fpu_fma(operands);
               rg_stage_valid[0] <= 1;
      endmethod
      method ReturnType#(8,23) receive();
         return ReturnType{valid:rg_stage_valid[`STAGES_FMA_SP-1],value:tpl_1(rg_stage_out[`STAGES_FMA_SP-1]) ,ex:tpl_2(rg_stage_out[`STAGES_FMA_SP-1])};
      endmethod 
   endmodule
   //
   (*synthesize*)
   module mk_fpu_fma_dp(Ifc_fpu_fma_dp);
      Vector#(`STAGES_FMA_DP,Reg#(Tuple2#(FloatingPoint#(11,52),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
      Vector#(`STAGES_FMA_DP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= `STAGES_FMA_DP -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
      method Action send(Tuple4#(
            FloatingPoint#(11,52),
            FloatingPoint#(11,52),
            Maybe#(FloatingPoint#(11,52)),
            RoundMode) operands);		
               rg_stage_out[0] <= fn_fpu_fma(operands);
               rg_stage_valid[0] <= 1;

      endmethod
      method ReturnType#(11,52) receive();
         return ReturnType{valid:rg_stage_valid[`STAGES_FMA_DP-1],value:tpl_1(rg_stage_out[`STAGES_FMA_DP-1]) ,ex:tpl_2(rg_stage_out[`STAGES_FMA_DP-1])};
      endmethod 
   endmodule
`endif
/* module mk_Tb();
    Ifc_fpu_fma#(8,23) ifc <- mk_fpu_fma();
    Reg#(int) cycle <- mkReg(0);
    Reg#(FloatingPoint#(8,23)) op3 <-mkReg(defaultValue);//'h3f800000;
    Reg#(FloatingPoint#(8,23)) op1 <-mkReg(defaultValue);
    Reg#(FloatingPoint#(8,23)) op2 <-mkReg(defaultValue);
  rule count_cycle;
    cycle <= cycle + 1;
    if(cycle>10)
    begin
       $finish(0);
    end
  endrule
    rule rlsend;
       // op2 <= op2+1;
       if(cycle==1) begin
          FloatingPoint#(8,23) op1 = FloatingPoint {
                sign:       False,
                exp:        8'b10000000,
                sfd:        23'b00000000000000000000000
             }; // decimal value = 2
          FloatingPoint#(8,23) op2 = FloatingPoint {
             sign:       False,
             exp:        8'b10000000,
             sfd:        23'b00000000000000000000000
          };
          FloatingPoint#(8,23) op3 = FloatingPoint {
             sign:       False,
             exp:        8'b10000000,
             sfd:        23'b00000000000000000000000
          };
          // op1 <= op1+2;
          // op3 <= op3+4;
         
          RoundMode op4 = Rnd_Nearest_Even;
          ifc.send(tuple4(tagged Valid op1,op2,op3,op4));
       end
    endrule
    rule receive;
//       match {.valid, .out} = ifc.receive();
       let x = ifc.receive();
       $display("%d : Valid %b => Value %b => Exc %b",cycle,x.valid,x.value,x.ex);
    endrule
 endmodule*/
endpackage
