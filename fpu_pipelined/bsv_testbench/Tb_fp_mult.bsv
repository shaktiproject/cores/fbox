//////////////////////////////
// see LICENSE.iitm
//////////////////////////////
/* 
--------------------------------------------------------------------------------------------------
Author: Neel Gala
Email id: neelgala@gmail.com
Details:
--------------------------------------------------------------------------------------------------
*/
package Tb_fp_mult; 
//`include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;

  import RegFile :: * ;

  import fpu_multiplier_common :: * ;
  import fpu_booth_multiplier :: *;
  import fpu_common ::*;





  (*synthesize*)
  module mktb_mult_sp(Empty);
    RegFile#(Bit#(23) , Bit#(104)) stimulus <- mkRegFileLoad("test_mul_rnear_even_sp.txt", 0,4999999);
//    let fadd <- mk_fpu_add_sub_sp_instance;
    let fadd <- mk_fpu_multiplier_sp();

    

    Reg#(Bit#(23)) read_index <- mkReg(0);
    Reg#(int) count <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);

    Reg#(Bit#(32)) e_output <- mkReg(0);
    Reg#(Bit#(8)) e_flags <- mkReg(0);

    Reg#(Bit#(32)) e_output4 <- mkReg(0);
    Reg#(Bit#(8)) e_flags4 <- mkReg(0);

    Reg#(Bit#(32)) e_output7 <- mkReg(0);
    Reg#(Bit#(8)) e_flags7 <- mkReg(0);
    Reg#(Bit#(8)) e_flags2 <- mkReg(0);
    Reg#(Bit#(32)) e_output2 <- mkReg(0);
    Reg#(Bit#(8)) exp_flags <- mkReg(0);
    Reg#(Bit#(8)) exp_flags_d <- mkReg(0);
    Reg#(Bit#(32)) exp_output <- mkReg(0);
    Reg#(Bit#(32)) exp_output_d <- mkReg(0);
    Reg#(Bit#(32)) match_counter <- mkReg(0);
    Reg#(Bit#(32)) match_counter1 <- mkReg(0);

    rule match_counter_display (read_index==4999999);
      $display("%d output mismatches",match_counter);
      $display("%d flag mismatches",match_counter1);
    endrule

    rule counter;
      if(count==4)
        _ready<=True;
      else
      count<=count+1;
    endrule

    /*doc:rule: */
    rule rl_pick_stimulus_entry;

      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags1 = truncate(_e);
      _e = _e >> 8;
      Bit#(32) e_out1 = truncate(_e);
      _e = _e >> 32;
      Bit#(32) _inp2 = truncate(_e);
      _e = _e >> 32;
      Bit#(32) _inp1 = truncate(_e);

      e_output <= e_out1;
      e_flags <= e_flags1;

      let op1 = FloatingPoint{sign : unpack(_inp1[31]), exp: _inp1[30:23], sfd: _inp1[22:0]};
      let op2 = FloatingPoint{sign : unpack(_inp2[31]), exp: _inp2[30:23], sfd: _inp2[22:0]};
      //      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, output %h", read_index, op1, op2,e_out1))
      fadd.send(tuple3(op1, op2, Rnd_Nearest_Even));            

      read_index <= read_index + 1;
      if(read_index == 4999999)
        $finish(0);
    endrule



    rule r1;
      e_output4<=e_output;
      e_flags4<=e_flags;
    endrule
    rule r4;
      e_output2<=e_output4;
      e_flags2<=e_flags4;
    endrule
    rule r2;
      e_output7<=e_output2;
      e_flags7<=e_flags2;
    endrule
    rule r6;
      exp_output<=e_output7;
      exp_flags<=e_flags7;
    endrule
    
    /*doc:rule: */
    rule rl_check_output(_ready==True);
      //      match {.valid,.out,.flags} = fadd.receive();
      let x = fadd.receive();
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      if(valid== 1'b1) begin
      // let {e_out, e_flags} = ff_golden_output.first;
      Bit#(32) _out = {pack(out.sign), out.exp, out.sfd};
      //  ff_golden_output.deq;
      if( _out != pack(exp_output)) begin
      //      `logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h,flag %h",read_index, pack(exp_output), _out,flags))
      //          $finish(0);
        $display("TB: Outputs mismatch[%d]. Exp:%h Rcv:%h,flag %h",read_index, pack(exp_output), _out,flags);
        match_counter <= match_counter+1;
      end
      else begin
	       
        //  `logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h", read_index,pack(exp_output), _out))
      end
      //  golden_index <= golden_index + 1;
      if (flags != unpack(truncate(pack(exp_flags)))) begin
      //   `logLevel( tb, 0, $format("TB: Flags mismatch[%d]. G:%h R:%h output G: %h , R: %h ",read_index, pack(exp_flags), flags,pack(exp_output),_out))
       $display("TB: Flags mismatch[%d]. Exp:%h Rcv:%h output Exp: %h , Rcv: %h ",read_index, pack(exp_flags), flags,pack(exp_output),_out);
      //          $finish(0);
      match_counter1 <= match_counter1 + 1;
      end
      end
    endrule
  endmodule


  (*synthesize*)
  module mktb_mult_dp(Empty);
    RegFile#(Bit#(23) , Bit#(200)) stimulus <- mkRegFileLoad("test_mul_rnear_even_dp.txt", 0,4999999);
    //    let fadd <- mk_fpu_add_sub_sp_instance;
    let fadd <- mk_fpu_multiplier_dp();



    Reg#(Bit#(23)) read_index <- mkReg(0);

    Reg#(int) count <- mkReg(0);
    Reg#(Bool) _ready <- mkReg(False);

    Reg#(Bit#(64)) e_output <- mkReg(0);
    Reg#(Bit#(8)) e_flags <- mkReg(0);

    Reg#(Bit#(64)) e_output4 <- mkReg(0);
    Reg#(Bit#(8)) e_flags4 <- mkReg(0);

    Reg#(Bit#(64)) e_output7 <- mkReg(0);
    Reg#(Bit#(8)) e_flags7 <- mkReg(0);
    Reg#(Bit#(64)) e_output8 <- mkReg(0);
    Reg#(Bit#(8)) e_flags8 <- mkReg(0);
    Reg#(Bit#(8)) e_flags2 <- mkReg(0);
    Reg#(Bit#(64)) e_output2 <- mkReg(0);
    Reg#(Bit#(8)) exp_flags <- mkReg(0);
    Reg#(Bit#(64)) exp_output <- mkReg(0);
    Reg#(Bit#(8)) exp_flags_d <- mkReg(0);
    Reg#(Bit#(64)) exp_output_d <- mkReg(0);
    Reg#(Bit#(8)) exp_flags_d1 <- mkReg(0);
    Reg#(Bit#(64)) exp_output_d1 <- mkReg(0);
    Reg#(Bit#(32)) match_counter <- mkReg(0);
    Reg#(Bit#(32)) match_counter1 <- mkReg(0);

    rule counter;
      if(count==5)
        _ready<=True;
      else
    count<=count+1;
    endrule

    /*doc:rule: */
    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) e_flags1 = truncate(_e);
      _e = _e >> 8;
      Bit#(64) e_out1 = truncate(_e);
      _e = _e >> 64;
      Bit#(64) _inp2 = truncate(_e);
      _e = _e >> 64;
      Bit#(64) _inp1 = truncate(_e);
      
      e_output <= e_out1;
      e_flags <= e_flags1;
      
      let op1 = FloatingPoint{sign : unpack(_inp1[63]), exp: _inp1[62:52], sfd: _inp1[51:0]};
      let op2 = FloatingPoint{sign : unpack(_inp2[63]), exp: _inp2[62:52], sfd: _inp2[51:0]};
    //      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: op1:%h op2:%h, output %h", read_index, op1, op2,e_out1))
      fadd.send(tuple3(op1, op2, Rnd_Nearest_Even));            

      read_index <= read_index + 1;
      if(read_index == 4999999)
        $finish(0);
    endrule

    rule match_counter_display (read_index==4999999);
      $display("%d output mismatches",match_counter);
      $display("%d flag mismatches",match_counter1);
    endrule

    rule r1;
      e_output4<=e_output;
      e_flags4<=e_flags;
    endrule
    rule r4;
      e_output2<=e_output4;
      e_flags2<=e_flags4;
    endrule
    rule r2;
      e_output7<=e_output2;
      e_flags7<=e_flags2;
    endrule
    rule r7;
      e_output8<=e_output7;
      e_flags8<=e_flags7;
    endrule
    rule r8;
      exp_output_d<=e_output8;
      exp_flags_d<=e_flags8;
    endrule
    rule ro;
      exp_output_d1<=exp_output_d;
      exp_flags_d1<=exp_flags_d;
    endrule
    rule rf;
      exp_output<=exp_output_d1;
      exp_flags<=exp_flags_d1;
    endrule



    /*doc:rule: */
    rule rl_check_output(_ready==True);
    //      match {.valid,.out,.flags} = fadd.receive();
      let x = fadd.receive();
      let valid = x.valid;
      let out = x.value;
      let flags = x.ex;
      if(valid== 1'b1) begin
       // let {e_out, e_flags} = ff_golden_output.first;
        Bit#(64) _out = {pack(out.sign), out.exp, out.sfd};
      //  ff_golden_output.deq;
        if( _out != pack(exp_output)) begin
    //      `logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h,flag %h",read_index, pack(exp_output), _out,flags))
    //          $finish(0);
          $display("TB: Outputs mismatch[%d]. Exp:%h Rcv:%h,flag %h",read_index, pack(exp_output), _out,flags);
           match_counter <= match_counter+1;
        end
        else begin
          //  `logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h", read_index,pack(exp_output), _out))
        end
      //  golden_index <= golden_index + 1;
        if (flags != unpack(truncate(pack(exp_flags)))) begin
       //   `logLevel( tb, 0, $format("TB: Flags mismatch[%d]. G:%h R:%h output G: %h , R: %h ",read_index, pack(exp_flags), flags,pack(exp_output),_out))
         $display("TB: Flags mismatch[%d]. Exp:%h Rcv:%h output Exp: %h , Rcv: %h ",read_index, pack(exp_flags), flags,pack(exp_output),_out);
         match_counter1 <= match_counter1 + 1;
    //          $finish(0);
        end
      end
    endrule
  endmodule
endpackage

