////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
package fpu_booth_multiplier_backup;

import fpu_multiplier_common ::*;
import DReg ::*;


`ifdef fpu_hierarchical
	interface Ifc_fpu_multiplier_sp;                                                // single precision Multiplier Interface
method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);
method ReturnType#(8,23) receive();
endinterface
	
	interface Ifc_fpu_multiplier_dp;                                                // double precision Multiplier Interface
method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);
method ReturnType#(11,52) receive();
endinterface
`endif



`ifdef fpu_hierarchical

(*synthesize*)
module mk_fpu_multiplier_sp(Ifc_fpu_multiplier_sp);                                         // single precision Multiplier Module


//registers
Reg#(Bit#(3)) rg_sp_ex_1<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex0<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex1<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex2<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex3<-mkReg(0);
Reg#(Bit#(3)) rg_sp_ex4<-mkReg(0);
Reg#(Bit#(1)) rg_s0<-mkReg(0);
Reg#(Bit#(1)) rg_s1<-mkReg(0);
Reg#(Bit#(1)) rg_valid<-mkReg(0);
Reg#(Tuple4#(Bit#(8),Int#(32), Bit#(5),Bit#(2))) rg_e0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(48),Bit#(8),Bit#(5),Bit#(2))) rg_x0<-mkReg(tuple4(0,0,0,0));
Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc0 <- mkReg(defaultValue);
Reg#(Exception) rg_exc1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc2 <- mkReg(defaultValue);
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands3 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands4 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands5 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands6 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands7 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode)) rg_operands8 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_1 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out_2 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple3#(Bit#(10),Bit#(1),Int#(32))) rg_e1 <- mkReg(tuple3(0,0,unpack(0)));
Reg#(Bit#(48)) rg_pp0 <- mkReg(0);
Reg#(Bit#(48)) rg_pp1 <- mkReg(0);
Reg#(Bit#(48)) rg_pp2 <- mkReg(0);
Reg#(Bit#(48)) rg_pp3 <- mkReg(0);

Vector#(4,Reg#(Bit#(1))) rg_sp_valid <- replicateM(mkDReg(0));


rule r_exp;                                         
match {.opA, .opB, .rmode} = rg_operands1;

rg_e0 <= fn_find_exp_sp(opA.exp,opB.exp,opA.sfd,opB.sfd);              //evaluating exponent, 1st stage
rg_s0<= pack(opA.sign != opB.sign);                                 //evaluating sign, 1st stage
endrule


rule r_ex;
match {.opA, .opB, .rmode} = rg_operands2;

rg_rnd0<= rmode;  
rg_sp_ex0<= fn_Special_EX_sp(opA.exp,opB.exp,opA.sfd,opB.sfd);         //evaluating special exceptions,1st stage
endrule




rule r_pp1;                  
match {.opA, .opB, .rmode} = rg_operands3;               // evaluating partial product1 for mantissa, 1st stage

Bit#(8) expA = 0;
Bit#(8) expB = 0;
Bit#(23) sfdA = 0;
Bit#(23) sfdB = 0;


sfdA = opA.sfd;
expA = opA.exp;
sfdB = opB.sfd;
expB = opB.exp;

 rg_pp0<=fn_gen_pp_sp({(|expA),sfdA},sfdB[5:0]);

endrule                                                       
    
    
    
 rule r_pp2;                                             // evaluating partial product2 for mantissa, 1st stage
 
 match {.opA, .opB, .rmode} = rg_operands4; 
 Bit#(8) expA1 = 0;
Bit#(8) expB1 = 0;
Bit#(23) sfdA1 = 0;
Bit#(23) sfdB1 = 0;


sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
 rg_pp1<= {fn_gen_pp_sp({(|expA1),sfdA1},sfdB1[11:6])[41:0],6'd0};

 endrule   
          
          
 rule r_pp3;                                            // evaluating partial product3 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands5; 
 Bit#(8) expA1 = 0;
Bit#(8) expB1 = 0;
Bit#(23) sfdA1 = 0;
Bit#(23) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
 rg_pp2<={fn_gen_pp_sp({(|expA1),sfdA1},sfdB1[17:12])[35:0],12'd0};
 endrule            
         
 rule r_pp4;                                             // evaluating partial product4 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands6; 
 Bit#(8) expA1 = 0;
Bit#(8) expB1 = 0;
Bit#(23) sfdA1 = 0;
Bit#(23) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp3<={fn_gen_pp_sp({(|expA1),sfdA1},{(|expB1),sfdB1[22:18]})[29:0],18'd0};
 endrule             
         


rule r_norm;                                      // evaluating mantissa products,exception generation and normalisation, 2nd stage
rg_sp_ex1<=rg_sp_ex0;
rg_s1<=(tpl_3(rg_e0)[4]==1'b1)?1'b0:rg_s0;
rg_x0<= fn_norm_sp(pack(rg_pp0),pack(rg_pp1),pack(rg_pp2),pack(rg_pp3),tpl_1(rg_e0),tpl_2(rg_e0),tpl_3(rg_e0),tpl_4(rg_e0));
rg_rnd1<=rg_rnd0;
rg_exc2<=unpack(tpl_3(rg_e0));
endrule
 


(*no_implicit_conditions*)                                 //rounding off mantissa and finalising exponents and exceptions,3rd stage
rule r_round; 

rg_sp_ex2<=rg_sp_ex1;

case(rg_rnd1)

Rnd_Zero              : rg_out <= fn_rnd_Zero_sp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Nearest_Even      : rg_out <= fn_rnd_Nearest_Even_sp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Nearest_Away_Zero : rg_out <= fn_rnd_Nearest_Away_Zero_sp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Plus_Inf          : rg_out <= fn_rnd_Plus_Inf_sp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Minus_Inf         : rg_out <= fn_rnd_Minus_Inf_sp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

default               : rg_out <= tuple2(defaultValue,defaultValue);  
endcase 

endrule



rule r_valid;

for(Integer i = 1 ; i <= 3 ; i = i+1)
			begin
				
					rg_sp_valid[i] <= rg_sp_valid[i-1];
			end

endrule




// send and receive methods
method Action send(Tuple3#(FloatingPoint#(8,23),FloatingPoint#(8,23), RoundMode) data_in);

rg_operands1<=data_in;
rg_operands2<=data_in;
rg_operands3<=data_in;
rg_operands4<=data_in;
rg_operands5<=data_in;
rg_operands6<=data_in;
rg_sp_valid[0]<=unpack(1'b1);
endmethod


method ReturnType#(8,23) receive();                                      

return case (pack(rg_sp_ex2))
			3'b000:  ReturnType{valid: pack(rg_sp_valid[3]),value:tpl_1(rg_out),ex:tpl_2(rg_out)};
			3'b100:  ReturnType{valid: pack(rg_sp_valid[3]),value:FloatingPoint{sign: unpack(1'b0),exp: 8'hFF,sfd: truncate(24'h400000)},ex:unpack(5'b10000)};
			3'b010:  ReturnType{valid: pack(rg_sp_valid[3]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 8'hFF,sfd: 23'd0},ex:defaultValue};
			3'b001:  ReturnType{valid: pack(rg_sp_valid[3]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 8'd0,sfd: 23'd0},ex:defaultValue};
			3'b111:  ReturnType{valid: pack(rg_sp_valid[3]),value:FloatingPoint{sign: unpack(1'b0),exp: 8'hFF,sfd: truncate(24'h400000)},ex:unpack(5'b00000)};
			endcase;
endmethod

endmodule





	
(*synthesize*)
module mk_fpu_multiplier_dp(Ifc_fpu_multiplier_dp);                                         // single precision Multiplier Module


//registers
Reg#(Bit#(3)) rg_dp_ex_1<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex0<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex00<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex01<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex1<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex2<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex3<-mkReg(0);
Reg#(Bit#(3)) rg_dp_ex4<-mkReg(0);
Reg#(Bit#(1)) rg_s0<-mkReg(0);
Reg#(Bit#(1)) rg_s00<-mkReg(0);
Reg#(Bit#(1)) rg_s01<-mkReg(0);
Reg#(Bit#(1)) rg_s1<-mkReg(0);

Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e00<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple4#(Bit#(11),Int#(32), Bit#(5),Bit#(2))) rg_e01<-mkReg(tuple4(0,0,0,0));

Vector#(5,Reg#(Bit#(1))) rg_dp_valid <- replicateM(mkDReg(0));


Reg#(Tuple4#(Bit#(106),Bit#(11),Bit#(5),Bit#(2))) rg_x0<-mkReg(tuple4(0,0,0,0));
Reg#(Tuple3#(Bit#(13),Bit#(1),Int#(32))) rg_e1 <- mkReg(tuple3(0,0,unpack(0)));

Reg#(RoundMode) rg_rnd0 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd00 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd01 <- mkReg(defaultValue);
Reg#(RoundMode) rg_rnd1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc0 <- mkReg(defaultValue);
Reg#(Exception) rg_exc1 <- mkReg(defaultValue);
Reg#(Exception) rg_exc2 <- mkReg(defaultValue);

Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands1 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands2 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands3 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands4 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands5 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands6 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands7 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands8 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands9 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands10 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));
Reg#(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode)) rg_operands11 <- mkReg(tuple3(unpack(0),unpack(0),defaultValue));


Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_1 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_2 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out_3 <- mkReg(tuple2(unpack(0),unpack(0)));
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));

Reg#(Bit#(108)) rg_pp0 <- mkReg(0);
Reg#(Bit#(108)) rg_pp1 <- mkReg(0);
Reg#(Bit#(108)) rg_pp2 <- mkReg(0);
Reg#(Bit#(108)) rg_pp3 <- mkReg(0);
Reg#(Bit#(108)) rg_pp4 <- mkReg(0);
Reg#(Bit#(108)) rg_pp5 <- mkReg(0);
Reg#(Bit#(108)) rg_pp6 <- mkReg(0);
Reg#(Bit#(108)) rg_pp7 <- mkReg(0);
Reg#(Bit#(108)) rg_pp8 <- mkReg(0);
Reg#(Bit#(108)) res <- mkReg(0);
Reg#(Bit#(108)) res1 <- mkReg(0);
Reg#(Bit#(1)) rg_dp_valid <- mkReg(0);

rule r_exp;                                         
match {.opA, .opB, .rmode} = rg_operands1;

rg_e0 <= fn_find_exp_dp(opA.exp,opB.exp,opA.sfd,opB.sfd);              //evaluating exponent, 1st stage
rg_s0<= pack(opA.sign != opB.sign);                                 //evaluating sign, 1st stage
endrule


rule r_ex;
match {.opA, .opB, .rmode} = rg_operands2;

rg_rnd0<= rmode;  
rg_dp_ex0<= fn_Special_EX_dp(opA.exp,opB.exp,opA.sfd,opB.sfd);         //evaluating special exceptions,1st stage
endrule




rule r_pp1;                  
match {.opA, .opB, .rmode} = rg_operands3;               // evaluating partial product1 for mantissa, 1st stage

 Bit#(11) expA = 0;
Bit#(11) expB = 0;
Bit#(52) sfdA = 0;
Bit#(52) sfdB = 0;


sfdA = opA.sfd;
expA = opA.exp;
sfdB = opB.sfd;
expB = opB.exp;

 rg_pp0<=fn_gen_pp_dp({1'b0,(|expA),sfdA},sfdB[5:0]);

endrule                                                       
    
    
    
 rule r_pp2;                                             // evaluating partial product2 for mantissa, 1st stage
 
 match {.opA, .opB, .rmode} = rg_operands4; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;


sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
 rg_pp1<= {fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[11:6])[101:0],6'd0};

 endrule   
          
          
 rule r_pp3;                                            // evaluating partial product3 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands5; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
 rg_pp2<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[17:12])[95:0],12'd0};
 endrule            
         
 rule r_pp4;                                             // evaluating partial product4 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands6; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp3<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[23:18])[89:0],18'd0};
 endrule             
  
  
  
 rule r_pp5;                                             // evaluating partial product5 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands7; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp4<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[29:24])[83:0],24'd0};
 endrule     
  
  
 rule r_pp6;                                             // evaluating partial product6 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands8; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp5<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[35:30])[77:0],30'd0};
 endrule     
  
 rule r_pp7;                                             // evaluating partial product7 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands9; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp6<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[41:36])[71:0],36'd0};
 endrule    
 
  rule r_pp8;                                             // evaluating partial product8 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands10; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp7<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},sfdB1[47:42])[65:0],42'd0};
 endrule    
 
  rule r_pp9;                                             // evaluating partial product9 for mantissa, 1st stage
 match {.opA, .opB, .rmode} = rg_operands11; 
 Bit#(11) expA1 = 0;
Bit#(11) expB1 = 0;
Bit#(52) sfdA1 = 0;
Bit#(52) sfdB1 = 0;
sfdA1 = opA.sfd;
expA1 = opA.exp;
sfdB1 = opB.sfd;
expB1 = opB.exp;
rg_pp8<={fn_gen_pp_dp({1'b0,(|expA1),sfdA1},{1'b0,(|expB1),sfdB1[51:48]})[59:0],48'd0};
 endrule      
  
  
 rule r_add;               

Bit#(108) v1 =0;
Bit#(108) v2 =0;
Bit#(108) v3 =0;
Bit#(108) v4 =0;
Bit#(108) v5 =0;
Bit#(108) v6 =0;
Bit#(108) v7 =0;
Bit#(108) v8 =0;
Bit#(108) v9 =0;

v1 = pack(rg_pp0);
v2 = pack(rg_pp1);
v3 = pack(rg_pp2);
v4 = pack(rg_pp3);
v5 = pack(rg_pp4);
v6 = pack(rg_pp5);
v7 = pack(rg_pp6);
v8 = pack(rg_pp7);
v9 = pack(rg_pp8);

res1<= v1+v2+v3+v4+v5+v6+v7+v8+v9;
rg_dp_ex01<=rg_dp_ex0; 
rg_s01<=rg_s0;
rg_rnd01<=rg_rnd0; 
rg_e01<=rg_e0;
 
 endrule 

/*  
 rule r_p;
  rg_s01<=rg_s00;
  rg_dp_ex01<=rg_dp_ex00;
  res1<=res;
  rg_rnd01<=rg_rnd00;
  rg_e01<=rg_e00;
 endrule
*/ 
  

rule r_norm;                                      // evaluating mantissa products,exception generation and normalisation, 2nd stage
rg_dp_ex1<=rg_dp_ex01;
rg_s1<=(tpl_3(rg_e01)[4]==1'b1)?1'b0:rg_s01;
rg_x0<= fn_norm_dp(pack(res1),tpl_1(rg_e01),tpl_2(rg_e01),tpl_3(rg_e01),tpl_4(rg_e01));
rg_rnd1<=rg_rnd01;
rg_exc2<=unpack(tpl_3(rg_e01));
endrule
 


(*no_implicit_conditions*)                                 //rounding off mantissa and finalising exponents and exceptions,3rd stage
rule r_round; 

rg_dp_ex2<=rg_dp_ex1;

case(rg_rnd1)

Rnd_Zero              : rg_out <= fn_rnd_Zero_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Nearest_Even      : rg_out <= fn_rnd_Nearest_Even_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Nearest_Away_Zero : rg_out <= fn_rnd_Nearest_Away_Zero_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Plus_Inf          : rg_out <= fn_rnd_Plus_Inf_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

Rnd_Minus_Inf         : rg_out <= fn_rnd_Minus_Inf_dp(rg_exc2,pack(rg_s1),tpl_2(rg_x0),tpl_1(rg_x0),tpl_3(rg_x0),tpl_4(rg_x0));

default               : rg_out <= tuple2(defaultValue,defaultValue);  
endcase 

endrule




for(Integer i = 1 ; i <= 4 ; i = i+1)
			begin
				
					rg_dp_valid[i] <= rg_dp_valid[i-1];
			end

endrule


// send and receive methods
method Action send(Tuple3#(FloatingPoint#(11,52),FloatingPoint#(11,52), RoundMode) data_in);

rg_operands1<=data_in;
rg_operands2<=data_in;
rg_operands3<=data_in;
rg_operands4<=data_in;
rg_operands5<=data_in;
rg_operands6<=data_in;
rg_operands7<=data_in;
rg_operands8<=data_in;
rg_operands9<=data_in;
rg_operands10<=data_in;
rg_operands11<=data_in;
rg_dp_valid[0]<=unpack(1'b1);


endmethod


method ReturnType#(11,52) receive();                                      

return case (pack(rg_dp_ex2))
			3'b000:  ReturnType{valid: pack(rg_dp_valid[4]),value:tpl_1(rg_out),ex:tpl_2(rg_out)};
			3'b100:  ReturnType{valid: pack(rg_dp_valid[4]),value:FloatingPoint{sign: unpack(1'b0),exp: 11'd2047,sfd:  truncate(52'h8000000000000)},ex:unpack(5'b10000)};
			3'b010:  ReturnType{valid: pack(rg_dp_valid[4]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 11'd2047,sfd: 52'd0},ex:defaultValue};
			3'b001:  ReturnType{valid: pack(rg_dp_valid[4]),value:FloatingPoint{sign: tpl_1(rg_out).sign,exp: 11'd0,sfd: 52'd0},ex:defaultValue};
			3'b111:  ReturnType{valid: pack(rg_dp_valid[4]),value:FloatingPoint{sign: unpack(1'b0),exp: 11'd2047,sfd:  truncate(52'h8000000000000)},ex:unpack(5'b00000)};
			endcase;

endmethod

endmodule








`endif

endpackage
