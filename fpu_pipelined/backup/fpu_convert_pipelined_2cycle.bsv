////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Joyanta Mondal
Email id: ee19m058@smail.iitm.ac.in
Details: 2-cycle version of sp to dp and dp to sp converters
--------------------------------------------------------------------------------------------------
*/
package fpu_convert_pipelined_2cycle;

import Vector ::*;
import Vector::*;
import DefaultValue      ::*;
import DReg ::*;
import fpu_common ::*;


// conversion functions


interface Ifc_fpu_convert_sp_to_dp;
method Action start(Tuple2#(FloatingPoint#(8,23),RoundMode) operand);
method ReturnType#(11,52) receive();
endinterface

(*synthesize*)
module mk_fpu_convert_sp_to_dp(Ifc_fpu_convert_sp_to_dp);

Reg#(Bit#(1)) rg_sign <-mkReg(0);
Reg#(Bit#(11)) rg_exp <-mkReg(0);
Reg#(Bit#(52)) rg_sfd <-mkReg(0); 
Reg#(Tuple2#(FloatingPoint#(11,52),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));
Vector#(1,Reg#(Bit#(1))) rg_valid <- replicateM(mkDReg(0));


method Action start(Tuple2#(FloatingPoint#(8,23),RoundMode) operand);
rg_out <= fn_convert_sp_to_dp(operand);
rg_valid[0]<=unpack(1'b1);
endmethod

method ReturnType#(11,52) receive();
let x = ReturnType{valid:pack(rg_valid[0]) ,value:tpl_1(rg_out) ,ex:tpl_2(rg_out)};
	return x;
endmethod

endmodule


interface Ifc_fpu_convert_dp_to_sp;                                                 //interface for double-float conversion
 method Action start(Tuple2#(FloatingPoint#(11,52),RoundMode) operand);
 method ReturnType#(8,23) receive();
endinterface

(*synthesize*)
module mk_fpu_convert_dp_to_sp(Ifc_fpu_convert_dp_to_sp);                           //module for double-float conversion


 Reg#(Bit#(1)) rg_sign <-mkReg(0);
 Reg#(Bit#(8)) rg_exp <-mkReg(0);
 Reg#(Bit#(23)) rg_sfd <-mkReg(0); 
 Reg#(Tuple2#(FloatingPoint#(8,23),Exception)) rg_out <- mkReg(tuple2(unpack(0),unpack(0)));

 Vector#(2,Reg#(Bit#(1))) rg_valid <- replicateM(mkDReg(0));

 method Action start(Tuple2#(FloatingPoint#(11,52),RoundMode) operand);
  rg_out <= fn_convert_dp_to_sp(operand);                                          //double-float conversion function
  rg_valid[0]<=1'b1;
 endmethod

 method ReturnType#(8,23) receive();
  let x = ReturnType{valid:pack(rg_valid[0]) ,value:tpl_1(rg_out) ,ex:tpl_2(rg_out)};
  return x;
 endmethod

endmodule


//float-double conversion function

function Tuple2#(FloatingPoint#(11,52),Exception) fn_convert_sp_to_dp(Tuple2#(FloatingPoint#(8,23),RoundMode) operand);

 FloatingPoint#(11,52) out = defaultValue;                    //double precision result
 Exception ex = defaultValue;                                 //double precision exception
 Bit#(5) flag = 0;

 match {.in,.roundmode} = operand;

 Bit#(23) sgfd = in.sfd;
 Bit#(8) expn = in.exp;
 flag[0] = pack((|expn==1'b0)&&(|sgfd==1'b0));           //zero flag
 flag[1] = pack((&expn==1'b1)&&(|sgfd==1'b0));            //infinity flag
 flag[2] = pack((&expn==1'b1)&&(sgfd[22]==1'b1));         //qnan flag
 flag[3] = pack((&expn==1'b1)&&(sgfd[22]==1'b0)&&(|sgfd[21:0]==1'b1));   //snan flag

 Bit#(11) out_exp = zeroExtend(expn)+ 'd896;           //evaluating exponent
 Bit#(52) out_sfd = {sgfd,29'd0};                      //zero-extending lsb for mantissa
/*
 Vector#(52, Bit#(1)) v = unpack(out_sfd);                        //counting MSB 0's of product
 Integer result=0;                                   
 Bool done = False;
 for(Integer i = 0; i <52; i = i + 1) 
  begin
   if (v[i] == 1)  result = i+1 ;
   else
    done = True;
  end

 Integer i0 = (done==True)?52-result:0;           
*/

Bit#(52) inp = out_sfd;


      Vector#(13, Bit#(1)) v1 = unpack(inp[51:39]);                        //counting MSB 0's of input
   Integer result1=0;                                  
   Bool done1 = False;
   for( Integer p1 = 12; p1 >=0; p1 = p1 - 1) 
     begin
       if ((v1[p1] == 0)&&(!done1))  result1 = result1+1 ;
       else
         done1 = True;
     end
   //Integer i0 = (done1==True)?(16-result1):0;
   Integer z0 = (result1);
   
   Vector#(13, Bit#(1)) v2 = unpack(inp[38:26]);                        //counting MSB 0's of input
   Integer result2=0;                                   
   Bool done2 = False;
   for( Integer p2 = 12; p2 >=0; p2 = p2 - 1) 
     begin
       if ((v2[p2] == 0)&&(!done2))  result2 = result2+1 ;
       else
         done2 = True;
     end
   //Integer i1 = (done2==True)?(16-result2):0;
   Integer z1 = result2;
   
   Vector#(13, Bit#(1)) v3 = unpack(inp[25:13]);                        //counting MSB 0's of input
   Integer result3=0;                                   
   Bool done3 = False;
   for( Integer p3 = 12; p3 >=0; p3 = p3 - 1) 
     begin
       if ((v3[p3] == 0)&&(!done3))  result3 = result3+1 ;
       else
         done3 = True;
     end
   //Integer i2 = (done3==True)?(16-result3):0;
   Integer z2 = result3;
   
   Vector#(13, Bit#(1)) v4 = unpack(inp[12:0]);                        //counting MSB 0's of input
   Integer result4=0;                                   
   Bool done4 = False;
   for( Integer p4 = 12; p4 >=0; p4 = p4 - 1) 
     begin
       if ((v4[p4] == 0)&&(!done4))  result4 = result4+1 ;
       else
         done4 = True;
     end
   //Integer i3 = (done4==True)?(16-result4):0;
   Integer z3 = result4;
   
   Integer i0 = (|inp[51:39]==1)?z0:((|inp[38:26]==1)?(z1+13):((|inp[25:13]==1)?(z2+26):((|inp[12:0]==1)?(z3+39):52)));







 out.sign = (flag[2]==1'b1)||(flag[3]==1'b1)?unpack(1'b0):in.sign;   //evaluating output sign
 out.exp = (((flag[2]==1'b1)||(flag[3]==1'b1)||(flag[1]==1'b1))?11'd2047:((flag[0]==1'b1)?11'd0:(((i0>0)&&(|expn==0))?(out_exp-fromInteger(i0)):out_exp)));                                         //evaluating output exponent
 out.sfd = (flag[3]==1'b1)?{2'b10,50'd0}:((flag[2]==1'b1)?{1'b1,51'd0}:((flag[1]==1'b1)?52'd0:((flag[0]==1'b1)?52'd0:(((i0>0)&&(|expn==0))?out_sfd<<fromInteger(i0+1):((|expn==0)?out_sfd<<1:out_sfd)))));  //evaluating output normalised mantissa

 ex = (flag[3]==1'b1)?unpack(5'b10000):defaultValue;                //generating output exceptions
 return tuple2(out,ex);
endfunction


//double to float function------------------------------


function Tuple2#(FloatingPoint#(8,23),Exception) fn_convert_dp_to_sp(Tuple2#(FloatingPoint#(11,52),RoundMode) operand);

 FloatingPoint#(8,23) out = defaultValue;       //single precision result
 Exception ex = defaultValue;                   //single precision exception
 Bit#(5) ex_flag = 0;
 Bit#(4) flags = 0;
 Bit#(1) denormal = 0;

 match {.in,.roundmode} = operand;

 Bit#(52) sgfd = in.sfd;
 Bit#(11) expn = in.exp;

 flags[0] = pack((|expn==1'b0)&&(|sgfd==1'b0));           //zero flag
 flags[1] = pack((&expn==1'b1)&&(|sgfd==1'b0));            //infinity flag
 flags[2] = pack((&expn==1'b1)&&(sgfd[51]==1'b1));         //qnan flag
 flags[3] = pack((&expn==1'b1)&&(sgfd[51]==1'b0)&&(|sgfd[50:0]==1'b1));   //snan flag


 if (flags[0]==1) begin     //input is zero
  out = FloatingPoint{sign:in.sign , exp : 8'd0 , sfd : 23'd0};    //setting output as zero
 end
 else if (flags[2] == 1 || flags[3] == 1) begin  //input is SNAN or QNAN
  ex_flag[4] = flags[3];                                                     //setting NAN flag
  out = FloatingPoint{sign:unpack(1'b0) , exp : 8'hFF, sfd : {1'b1,22'd0}};  //setting output as QNAN
 end
 else if (flags[1] == 1) begin  //input is infinity
  out = FloatingPoint{sign:in.sign , exp : 8'hFF, sfd : 23'd0};          //setting output as infinity
 end
 else if (expn > 'd1150) begin        //overflow cases
 ex_flag[2] = 1;
 ex_flag[0] = 1;
 
  if(roundmode == Rnd_Zero) //Round to zero 
   out = FloatingPoint{sign:in.sign , exp : 8'hFE, sfd : {3'b111,20'hFFFFF}};  //Highest positive number 7f7fffff
   
  else if (roundmode == Rnd_Minus_Inf)  //Round down
    if(in.sign == False)
     out = FloatingPoint{sign:in.sign , exp : 8'hFE, sfd : {3'b111,20'hFFFFF}};
    else
     out = FloatingPoint{sign:in.sign , exp : 8'hFF, sfd : 23'd0};  
     
  else if (roundmode == Rnd_Plus_Inf && in.sign == True)  //round up
   out = FloatingPoint{sign:unpack(1'b1) , exp : 8'hFE, sfd : {3'b111,20'hFFFFF}};
  else
   out = FloatingPoint{sign:in.sign , exp : 8'hFF, sfd : 23'd0};  
  end
  else begin       //without overflow cases
   Bit#(32) res = 0;
   Bit#(49) man = 0;
   Bit#(8)  expo = 0;
   bit underflow = 0;
   bit lv_guard = 0;
   bit lv_denormal_roundup = 0;      
   let lv_sticky = |sgfd[26:0];     
   if (expn <= 'd872) begin    //Underflow cases
    if(roundmode == Rnd_Minus_Inf && in.sign == True) //Round Down
     res = {1'b1,30'b0,1'b1};
    else if(roundmode == Rnd_Plus_Inf && in.sign == False)
     res = 1;
    else
     res = {pack(in.sign),'0};
     underflow = 1;
     ex_flag[1] = 1;
     ex_flag[0] = 1;
    end
    else if ((expn <= 'd896)&&(expn > 'd872)) begin  //Denormal number //Set sticky bit!!
     let shiftDist = 'd896 - expn;
     man = {1'b1,sgfd[51:27],23'd0} >> shiftDist;
     if(man[23:0] != 0)
      lv_sticky = 1;
      expo = '0;
      denormal = 1;      
     end
     else begin      //Normal number cases
      expo = truncate(expn - 'd896);
      man = {sgfd[51:27],24'd0};
     end
 
    lv_guard = man[25];
    let lv_round = man[24];
    let lv_inexact = 0;
    let lv_round_up = 0;
    if(((lv_guard | lv_round | lv_sticky)) == 1) begin
     lv_inexact = 1;
     ex_flag[0]=1'b1;
    end
    if(denormal == 1 && lv_inexact == 1) begin
     ex_flag[1]=1;                                                        //setting underflow flag        
     ex_flag[0] = 1;                                                      //setting inexact flag
    end
    
    //setting round_up bit(rounding_off)
    if(flags[2]==0 && flags[1] == 0 &&  flags[3] == 0) begin
      if(roundmode == Rnd_Nearest_Even)   //roundmode nearest_even
	lv_round_up = lv_guard & (lv_round|lv_sticky|man[26]);
      else if(roundmode == Rnd_Nearest_Away_Zero)      //roundmode nearest_away_zero
	lv_round_up = lv_guard; 
      else if(roundmode == Rnd_Plus_Inf)               //roundmode plus_infinity(round_up)
	lv_round_up = (lv_guard|lv_round|lv_sticky) & ~pack(in.sign);
      else if(roundmode == Rnd_Minus_Inf)begin         //roundmode minus_infinity(round_down)
        lv_round_up = (lv_guard|lv_round|lv_sticky) & pack(in.sign);
      end
    Bit#(24) fman = zeroExtend(man[48:26]);   //rounded result
    //modifying exponent and mantissa        
      if(lv_round_up == 1)                     
       fman = fman + 1;                      //normalised result
      if(fman[23] == 1) begin
       expo = expo + 1;                                                    
       ex_flag[1] = (&sgfd[51:29]==1)&&(ex_flag[1]==1)?((denormal==1)&&(lv_round==0)&&((roundmode==Rnd_Nearest_Away_Zero)||(roundmode==Rnd_Nearest_Even))?1:((lv_sticky==0)?((lv_round==1)?0:1):0)):(ex_flag[1]);   //setting final underflow flag
      end
      if(underflow==0)                                    
       res = {pack(in.sign),expo,fman[22:0]};         //numbers without overflow or underflow
      end
    out = FloatingPoint{sign:unpack(res[31]),exp : res[30:23], sfd: res[22:0]};  //final result
    ex_flag[2]=ex_flag[2]|pack((&res[30:23]==1'b1)&&(|res[22:0]==1'b0));  //setting final overflow flag
   end   
 ex = unpack(ex_flag);                            //final exception
 return tuple2(out,ex);
endfunction

endpackage
