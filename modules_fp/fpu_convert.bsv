////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2011  Bluespec, Inc.   ALL RIGHTS RESERVED.
// $Revision$
// $Date$
////////////////////////////////////////////////////////////////////////////////
//see LICENSE.iitm
////////////////////////////////////////////////////////////////////////////////
/*
---------------------------------------------------------------------------------------------------

Author: Neel Gala, Sujay Pandit, Lokhesh Kumar
Email id: neelgala@gmail.com, contact.sujaypandit@gmail.com, lokhesh.kumar@gmail.com
--------------------------------------------------------------------------------------------------
*/
package fpu_convert;
`include "Logger.bsv"
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import UniqueWrappers    ::*;
import FIFOF             ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"

`define Reg_width 32
`define ELEN 64
`define floating_output_dp_width 64
`define floating_output_sp_width 32
//// ------Contains:
// Compare
// Class
// int to sp
// sp to int 
// int to dp -> Incomplete
// dp to int 
////

////////// TYPEDEFS 
// For Class functions
typedef struct{
		Bit#(width) final_result;					// the final result for the operation
		Bit#(5) fflags; 					// indicates if any exception is generated.
}Floating_output#(numeric type width) deriving(Bits, Eq); // data structure of the output FIFO.
//
//
///////////////////////////// 
/// Compare function
////////////////////////////
function Disorder fn_fpu_compare( FloatingPoint#(e,m) x, FloatingPoint#(e,m) y );
  if(isSNaN(x) || isSNaN(y))
     return SN;
  else if (isNaN(x) || isNaN(y))
     return UO;
  else if (isZero(x) && isZero(y))
     return EQ;
  else begin
     let expLT  = x.exp < y.exp;
     let expEQ  = x.exp == y.exp;
     let expGT  = x.exp > y.exp;
     let sfdLT  = x.sfd < y.sfd;
     let sfdGT  = x.sfd > y.sfd;
     let sfdEQ  = x.sfd == y.sfd;

     if (x.sign && !y.sign) begin
  return LT;
     end
     else if (!x.sign && y.sign) begin
  return GT;
     end
     else if (!x.sign) begin
  // both positive
  if (expLT || (expEQ && sfdLT)) begin
     return LT;
  end
  else if (expGT || (expEQ && sfdGT)) begin
     return GT;
  end
  else begin
     return EQ;
  end
     end
     else begin
  // both negative
  if (expGT || (expEQ && sfdGT)) begin
     return LT;
  end
  else if (expLT || (expEQ && sfdLT)) begin
     return GT;
  end
  else begin
     return EQ;
  end
     end
  end
endfunction

interface Ifc_fpu_compare#(numeric type e, numeric type m);
    method Disorder sendreceive(
    FloatingPoint#(e,m) x,
      FloatingPoint#(e,m) y
    );
endinterface
module mk_fpu_compare(Ifc_fpu_compare#(e, m)); // FOR SYNTH RUNS
    Reg#(Disorder) rg_result <- mkReg(EQ);
    method Disorder sendreceive(
            FloatingPoint#(e,m) x,
            FloatingPoint#(e,m) y
        );
                return fn_fpu_compare(x,y);
    endmethod
endmodule
(*synthesize*)
 module mk_fpu_compare_sp_instance(Ifc_fpu_compare#(8,23));
    let ifc();
    mk_fpu_compare _temp(ifc);
    return (ifc);
 endmodule

(*synthesize*)
 module mk_fpu_compare_dp_instance(Ifc_fpu_compare#(11,52));
    let ifc();
    mk_fpu_compare _temp(ifc);
    return (ifc);
 endmodule

////////////////////////////////////
/////////    F.MOV
////////////////////////////////////

/*
Operation(2 bit) 
00 -> FMV_X_W (S)
01 -> FMV_W_X (S)
10 -> FMV_X_W (D)
11 -> FMV_W_X (D)
*/

interface Ifc_fpu_mov;
//    method ActionValue#(Tuple2#(Bit#(`ELEN),Exception)) sendreceive(Bit#(`ELEN) operand,Bit#(2) operation); // for sp pass zeroExtend(pack(operand))
	method ReturnTypeInt#(`ELEN) sendreceive(Bit#(`ELEN) operand,Bit#(2) operation); // for sp pass zeroExtend(pack(operand))
endinterface
module mk_fpu_mov(Ifc_fpu_mov);
//    method Tuple2#(Bit#(`ELEN),Exception)) sendreceive(Bit#(`ELEN) operand,Bit#(2) operation);
    method ReturnTypeInt#(`ELEN) sendreceive(Bit#(`ELEN) operand,Bit#(2) operation);
        Bit#(`ELEN) lv_final_result = 0; // always a 64 bit outpu
        if(operation==2'b00) // sp to integer 
                lv_final_result = signExtend(operand[31:0]);
        else if(operation==2'b10) begin // integer to sp
            `ifdef dpfpu
                lv_final_result = {'1,operand[31:0]};
            `else
                lv_final_result= zeroExtend(operand[31:0]);
            `endif
        end
        else // RV64 (send out entire 64 bit floatingpoint as it is) 
            lv_final_result = operand;
        Exception lv_exc = defaultValue;
	let x = ReturnTypeInt{valid:1,value:lv_final_result,ex:lv_exc};
	return x;
//        return tuple2(lv_final_result,lv_exc);
    endmethod
endmodule

//****************************************************************Min start**********************************************************************************
////////////////////////////////////
/////////    Min
////////////////////////////////////

interface Ifc_fpu_min#(numeric type e, numeric type m);
	method ReturnType#(e,m) start(Tuple2#(FloatingPoint#(e,m),FloatingPoint#(e,m)) inp);
endinterface

module mk_fpu_min(Ifc_fpu_min#(e,m));
	method ReturnType#(e,m) start(Tuple2#(FloatingPoint#(e,m),FloatingPoint#(e,m)) inp);
        let x = tpl_1(inp);
	let y = tpl_2(inp);
	let lv_m = valueof(m);
	Bit#(m) z = 0;
	z[lv_m-1] = 1;
      if((isSNaN(x) || isQNaN(x)) && (isSNaN(y) || isQNaN(y))) return ReturnType{valid:1,value:FloatingPoint{sign:False,exp:'1,sfd:z},ex:(isSNaN(x) || isSNaN(y))?unpack('d16):unpack(0)};
      else if (isSNaN(x)) return ReturnType{valid:1,value:y,ex:unpack('d16)};
      else if (isSNaN(y)) return ReturnType{valid:1,value:x,ex:unpack('d16)};
      else if (isQNaN(x)) return ReturnType{valid:1,value:y,ex:unpack(0)};
      else if (isQNaN(y)) return ReturnType{valid:1,value:x,ex:unpack(0)};
      else begin
	 let signLT = (x.sign && !y.sign);	
	 let signEQ = x.sign == y.sign;		
	 let expLT  = x.exp < y.exp;		
	 let expGT  = x.exp > y.exp;	
	 let expEQ  = x.exp == y.exp;		
	 let manLT  = x.sfd < y.sfd;	
	 let manGT  = x.sfd > y.sfd;	
	 let signXpos  = !x.sign;
	 let signXneg  = x.sign;

	 if (signLT || (signEQ && expLT && signXpos) || (signEQ && expEQ && manLT && signXpos)) return ReturnType{valid:1,value:x,ex:unpack(0)};
	 else if ((signEQ && expGT && signXneg) || (signEQ && expEQ && manGT && signXneg)) return ReturnType{valid:1,value:x,ex:unpack(0)};
	 else return ReturnType{valid:1,value:y,ex:unpack(0)};
      end
      endmethod
endmodule

(*synthesize*)
 module mk_fpu_min_sp_instance(Ifc_fpu_min#(8,23));
    let ifc();
    mk_fpu_min _temp(ifc);
    return (ifc);
 endmodule

(*synthesize*)
 module mk_fpu_min_dp_instance(Ifc_fpu_min#(11,52));
    let ifc();
    mk_fpu_min _temp(ifc);
    return (ifc);
 endmodule
//****************************************************************Min end**********************************************************************************


//****************************************************************Max start**********************************************************************************
////////////////////////////////////
/////////    Max
////////////////////////////////////

interface Ifc_fpu_max#(numeric type e, numeric type m);
	method ReturnType#(e,m) start(Tuple2#(FloatingPoint#(e,m),FloatingPoint#(e,m)) inp);
endinterface

module mk_fpu_max(Ifc_fpu_max#(e,m));
	method ReturnType#(e,m) start(Tuple2#(FloatingPoint#(e,m),FloatingPoint#(e,m)) inp);
    let x = tpl_1(inp);
		let y = tpl_2(inp);
		let lv_m = valueof(m);
		Bit#(m) z = 0;
		z[lv_m-1] = 1;
		if((isSNaN(x) || isQNaN(x)) && (isSNaN(y) || isQNaN(y))) return ReturnType{valid:1,value:FloatingPoint{sign:False,exp:'1,sfd:z},ex:(isSNaN(x) || isSNaN(y))?unpack('d16):unpack(0)};
      else if (isSNaN(x)) return ReturnType{valid:1,value:y,ex:unpack('d16)};
      else if (isSNaN(y)) return ReturnType{valid:1,value:x,ex:unpack('d16)};
      else if (isQNaN(x)) return ReturnType{valid:1,value:y,ex:unpack(0)};
      else if (isQNaN(y)) return ReturnType{valid:1,value:x,ex:unpack(0)};
      else begin
	 let signEQ = x.sign == y.sign;
	 let signGT = (!x.sign && y.sign);
	 let expEQ  = x.exp == y.exp;
	 let expGT  = x.exp > y.exp;
	 let manGT  = x.sfd > y.sfd;
	 let signXpos  = !x.sign;
	 let signXneg  = x.sign;
	 let expLT  = x.exp < y.exp;
	 let manLT  = x.sfd < y.sfd;

	 if (signGT || (signEQ && expGT && signXpos) || (signEQ && expEQ && manGT && signXpos)) return ReturnType{valid:1,value:x,ex:unpack(0)};
	 else if ((signEQ && expLT && signXneg) || (signEQ && expEQ && manLT && signXneg)) return ReturnType{valid:1,value:x,ex:unpack(0)};
	 else return ReturnType{valid:1,value:y,ex:unpack(0)};
      end
      endmethod
endmodule

(*synthesize*)
 module mk_fpu_max_sp_instance(Ifc_fpu_max#(8,23));
    let ifc();
    mk_fpu_max _temp(ifc);
    return (ifc);
 endmodule

(*synthesize*)
 module mk_fpu_max_dp_instance(Ifc_fpu_max#(11,52));
    let ifc();
    mk_fpu_max _temp(ifc);
    return (ifc);
 endmodule
//*****************************************************************Max end*********************************************************************************


////////////////////////////////////
/////////    Class : F.Class , D.Class
////////////////////////////////////

// replacing fpman and fpexp with m and e 
// removing fpinp from arguments
interface Ifc_fpu_fclass#(numeric type e, numeric type m);
	method ReturnTypeInt#(`ELEN) start(Tuple2#(FloatingPoint#(e,m),Exception) inpfp);
endinterface

module mk_fpu_fclass(Ifc_fpu_fclass#(e,m));

	// let fPINP  = valueOf(fpinp);
	// let fPMAN  = valueOf(fpman);
	// let fPEXP  = valueOf(fpexp);
    Reg#(Tuple2#(Bit#(10),Exception)) rg_result <- mkReg(tuple2(unpack(0),unpack(0)));
    method ReturnTypeInt#(`ELEN) start(Tuple2#(FloatingPoint#(e,m),Exception) inpfp);
        
        match {.in,.flags} = inpfp;
		Bit#(10) result_fclass;
        Bool sbit = (pack(in.sign)==1);
        Bool inf  = (pack(flags)[1]==1);
        Bool normal = (pack(flags) == '0);
        Bool subnormal = (pack(flags)[4] == 1);
        Bool zero    = (pack(flags)[3] == 1);
		if(sbit && inf)  //negtive infinity
		begin	
		result_fclass = 'd1;	
		end

		else if(sbit && normal)  //negative normal
		begin	
		result_fclass = 'd2;
	    end
		
		else if(sbit && subnormal) //negative subnormal
		begin	
		result_fclass = 'd4;
		end

		else if(sbit && zero)  //-0
		begin
		result_fclass = 'd8;
		end

		else if(!sbit && zero) // +0
		begin
		result_fclass = 'd16;
		end

		else if( !sbit && subnormal) //positive subnormal	
		begin	
		result_fclass = 'd32;
	    end

		else if(!sbit && normal)  //positive normal
		begin
		result_fclass = 'd64;
		end

		else if(!sbit && inf) //positive infinity
		begin
		result_fclass = 'd128;	
		end

		else if (pack(flags)[0]==1 && in.sfd[valueOf(m)-1]==1'b0) //Signaling NaN	
		begin
  		result_fclass = 'd256; 
		end

		else  //quiet NaN	
		begin
		  result_fclass = 'd512; 
		end

        Exception exc = defaultValue;
	Bit#(`ELEN) temp = signExtend(result_fclass);
	let x = ReturnTypeInt{valid:1, value: temp,ex:exc};
        return  x;
	endmethod
endmodule

(*synthesize*)
 module mk_fpu_fclass_sp_instance(Ifc_fpu_fclass#(8,23));
    let ifc();
    mk_fpu_fclass _temp(ifc);
    return (ifc);
 endmodule

(*synthesize*)
 module mk_fpu_fclass_dp_instance(Ifc_fpu_fclass#(11,52));
    let ifc();
    mk_fpu_fclass _temp(ifc);
    return (ifc);
 endmodule

 
///////////////////////////// 
/// Dp to Sp or Sp to Dp
////////////////////////////
function Tuple2#(FloatingPoint#(e2,m2),Exception) fn_convert_spdp (Tuple3#(FloatingPoint#(e,m), RoundMode, Bool) operand)
   provisos(
      Max#(e, e2, emax),
      Max#(m, m2, mmax),
      Add#(emax, 1, expbits),
      Add#(mmax, 5, sfdbits),
      // per request of bsc
      Add#(a__, e, TAdd#(TMax#(e, e2), 1)),
      Add#(b__, e2, TAdd#(TMax#(e, e2), 1)),
      Add#(c__, TLog#(TAdd#(1, m)), expbits),
      Add#(d__, e2, expbits),
      Add#(m2, e__, sfdbits),
      Add#(f__, e, expbits),
      Add#(2, g__, sfdbits),
      Add#(1, TAdd#(1, TAdd#(m2, TAdd#(2, h__))), sfdbits),
      Add#(4, h__, e__),
      Add#(i__, TLog#(TAdd#(1, sfdbits)), TAdd#(e2, 1))
      );
      
   match {.in, .rmode, .preservenan} = operand;
   FloatingPoint#(e2,m2) out = defaultValue;
   Exception exc = defaultValue;

   if (isNaN(in)) begin
`ifdef denormal_support
      if (isSNaN(in) && !preservenan) begin
	 in = nanQuiet(in);
      end
`endif
      out.sign = False;
      out.exp = '1;
      out.sfd = zExtendLSB(2'b10);
//---------------------------------------------------------------------------------------------------
if(isSNaN(in))
      exc.invalid_op = True;
//---------------------------------------------------------------------------------------------------
//      exc.invalid_op = True;
`ifdef denormal_support
      Bit#(sfdbits) sfd = zExtendLSB(in.sfd);
      out.sfd = truncateLSB(sfd);
      if (out.sfd == 0)
	 out.sfd = zExtendLSB(2'b01);
`endif
   end
   else if (isInfinity(in))
      out = infinity(in.sign);
   else if (isZero(in))
      out = zero(in.sign);
   else if (isSubNormal(in)) begin
      Int#(expbits) exp = fromInteger(minexp(in));
      Bit#(sfdbits) sfd = zExtendLSB({1'b0, getHiddenBit(in),in.sfd});
      Int#(expbits) subexp = unpack(pack(extend(countZerosMSB(in.sfd))));

      if ((exp - subexp) > fromInteger(maxexp(out))) begin
	 out.sign = in.sign;
	 out.exp = maxBound - 1;
	 out.sfd = maxBound;

	 exc.overflow = True;
	 exc.inexact = True;

	 let y = round(rmode, out, '1);
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if ((exp - subexp) < fromInteger(minexp(out))) begin
	 out.sign = in.sign;
	 out.exp = 0;  // subnormal
`ifdef denormal_support
         //XXX XXX XXX Removed subnormal logic
	 let shift = fromInteger(abs(minexp(in)) - abs(minexp(out)));
	 if (shift < 0) begin
	    sfd = sfd << (-shift);
	 end
	 else if (shift < fromInteger(valueOf(sfdbits))) begin
	    Bit#(1) guard = |(sfd << (fromInteger(valueOf(sfdbits)) - shift));

	    sfd = sfd >> shift;
	    sfd[0] = sfd[0] | guard;
	 end
	 else if (|sfd == 1) begin
	    sfd = 1;
	 end

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);
`else
   out.sfd = 0;
`endif
	 if (isZero(out)) begin
	    exc.underflow = True;
	 end
`ifdef denormal_support
   let y = round(rmode, out, tpl_2(x));
`else
    let y = round(rmode, out, '0);
`endif
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
	 
      end
      else begin
	 out.sign = in.sign;
	 out.exp = pack(truncate(exp + fromInteger(bias(out))));

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);

	 let y = round(rmode, out, tpl_2(x));
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
     
      
   end
   else begin
      Int#(expbits) exp = signExtend(unpack(unbias(in)));
      Bit#(sfdbits) sfd = zExtendLSB({1'b0, getHiddenBit(in),in.sfd});

      if (exp > fromInteger(maxexp(out))) begin
	 out.sign = in.sign;
	 out.exp = maxBound - 1;
	 out.sfd = maxBound;

	 exc.overflow = True;
	 exc.inexact = True;

	 let y = round(rmode, out, '1);
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
      else if (exp < fromInteger(minexp(out))) begin
	 out.sign = in.sign;
    out.exp = 0;  // subnormal
    
`ifdef denormal_support
         //XXX XXX XXX Removed subnormal logic
	 let shift = (fromInteger(minexp(out)) - exp);
	 if (shift < fromInteger(valueOf(sfdbits))) begin
	    Bit#(1) guard = |(sfd << (fromInteger(valueOf(sfdbits)) - shift));

	    sfd = sfd >> shift;
	    sfd[0] = sfd[0] | guard;
	 end
	 else if (|sfd == 1) begin
	    sfd = 1;
	 end

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);
`else
   out.sfd = 0;
`endif
	 if (isZero(out)) begin
	    exc.underflow = True;
	 end
`ifdef denormal_support
   let y = round(rmode, out, tpl_2(x));
`else
    let y = round(rmode, out, '0);
`endif
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
         
      end
      else begin
	 out.sign = in.sign;
	 out.exp = pack(truncate(exp + fromInteger(bias(out))));

	 let x = normalize(out, sfd);
	 out = tpl_1(x);
	 exc = exc | tpl_3(x);

	 let y = round(rmode, out, tpl_2(x));
	 out = tpl_1(y);
	 exc = exc | tpl_2(y);
      end
   end
//-------------------------------------------------------------------------------------------------------------

if(valueOf(e) == 'd11)
begin
	let temp = in.sfd;
	temp = temp >> 28;
	if(out.exp ==1 && out.sfd == 0 && in.exp == 'b01110000000 && temp == 'h0000000ffffff)
	exc.underflow= False;
	let temp1 = in.sfd;
	temp1 = temp1>>29;
	
	if((rmode == Rnd_Minus_Inf || rmode == Rnd_Plus_Inf) && out.exp ==1 && out.sfd == 0 && in.exp == 'b01110000000 && temp1 == 'h00000007fffff && in.sfd != 'hfffffe0000000)
	  exc.underflow = False;
end

//-------------------------------------------------------------------------------------------------------------

   return tuple2(canonicalize(out), exc);
endfunction

interface Ifc_fpu_convert_spdp#(numeric type e,numeric type e2,numeric type m,numeric type m2,numeric type nos);
    method Action start(Tuple3#(FloatingPoint#(e,m), RoundMode, Bool) operand);
//    method Tuple2#(Bit#(1),Tuple2#(FloatingPoint#(e2,m2),Exception)) receive();
    method ReturnType#(e2,m2) receive();
endinterface
module mk_fpu_convert_spdp(Ifc_fpu_convert_spdp#(e,e2,m,m2,nos))
    provisos(
    Add#(a__, TLog#(TAdd#(1, m)), TAdd#(TMax#(e, e2), 1)),
    Add#(m2, b__, TAdd#(TMax#(m, m2), 5)),
    Add#(1, TAdd#(1, TAdd#(m2, TAdd#(2, c__))), TAdd#(TMax#(m, m2), 5)),
    Add#(d__, TLog#(TAdd#(1, TAdd#(TMax#(m, m2), 5))), TAdd#(e2, 1)),
    Add#(e__, e, TAdd#(TMax#(e, e2), 1)),
    Add#(f__, e2, TAdd#(TMax#(e, e2), 1))
    ); 
    
    Vector#(nos,Reg#(Tuple2#(FloatingPoint#(e2,m2),Exception))) rg_stage_out <- replicateM(mkReg(tuple2(unpack(0),unpack(0))));
    Vector#(nos,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= valueOf(nos) -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
    method Action start(Tuple3#(FloatingPoint#(e,m), RoundMode, Bool) operand);
                rg_stage_out[0] <= fn_convert_spdp(operand);
                rg_stage_valid[0] <= 1;
    endmethod
    method ReturnType#(e2,m2) receive();
	let x = ReturnType{valid:rg_stage_valid[valueOf(nos)-1] ,value:tpl_1(rg_stage_out[valueOf(nos)-1]) ,ex:tpl_2(rg_stage_out[valueOf(nos)-1])};
	return x;
    endmethod 
endmodule

 (*synthesize*)
 module mk_fpu_convert_sp_to_dp_instance(Ifc_fpu_convert_spdp#(8,11,23,52,`STAGES_FCONV));
    let ifc();
    mk_fpu_convert_spdp _temp(ifc);
    return (ifc);
 endmodule
 (*synthesize*)
 module mk_fpu_convert_dp_to_sp_instance(Ifc_fpu_convert_spdp#(11,8,52,23,`STAGES_FCONV));
    let ifc();
    mk_fpu_convert_spdp _temp(ifc);
    return (ifc);
 endmodule

///////////////////////////////
/// sp/dp to int (parameterised for values of 'e' and 'm')
//////////////////////////////

interface Ifc_fpu_sp_dp_to_int#(numeric type e, numeric type m, numeric type nos);
    // method ActionValue#(Floating_output#(`ELEN)) start(Bit#(1) sign,Bit#(8) exponent, Bit#(23) mantissa,  bit convert_unsigned, bit convert_long, Bit#(3) rounding_mode, Bit#(5) flags);
    method Action start(Tuple3#(FloatingPoint#(e,m),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
//    method Tuple2#(Bit#(1),Floating_output#(`ELEN)) receive();
    method ReturnTypeInt#(`ELEN) receive();
endinterface

module mk_fpu_sp_dp_to_int(Ifc_fpu_sp_dp_to_int#(e,m,nos))
provisos (
    Add#(a__, TAdd#(1, m), TAdd#(e, 64))
    );
    //Reg#(Floating_output#(`ELEN)) reslt <- mkReg(unpack(0));
    Vector#(nos,Reg#(Floating_output#(`ELEN))) rg_stage_out <- replicateM(mkReg(unpack(0)));
      Vector#(nos,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= valueOf(nos) -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule

    //old definition: method ActionValue#(Floating_output#(`ELEN)) start(Bit#(1) lv_sign,Bit#(8) lv_exponent, Bit#(23) lv_mantissa,  bit convert_unsigned, bit convert_long, Bit#(3) rounding_mode, Bit#(5) flags);
    method Action start(Tuple3#(FloatingPoint#(e,m),RoundMode,Exception) operands,bit convert_unsigned, bit convert_long);
        match {.in,.roundmode,.exc} = operands;
//      `logLevel( tb, 0, $format("input in module %h", in))
        let lv_sign = pack(in.sign);
        Integer lv_m_val = valueOf(m);
        Integer lv_e_val = valueOf(e);
        Bit#(e) lv_exponent = in.exp;
        Bit#(m) lv_mantissa = in.sfd;
        let rounding_mode = roundmode;
        let flags = pack(exc);
		bit lv_overflow = 0;
		bit lv_zero = flags[3];
		bit lv_infinity = flags[1];
        	bit lv_invalid = flags[0] | flags[2];
	        bit lv_denormal = flags[4];
        	bit lv_manzero = |lv_mantissa;
	        bit lv_inexact = 0;
        Bool to_round = False; 
        Bool rne = (rounding_mode == Rnd_Nearest_Even);
        Bool rtz = (rounding_mode == Rnd_Zero);
        Bool rdn = (rounding_mode == Rnd_Minus_Inf);
        Bool rup = (rounding_mode == Rnd_Plus_Inf);
        Bool rmm = (rounding_mode == Rnd_Nearest_Away_Zero);
        
        let lv_e_val_1 = lv_e_val+1;
        Bit#(TAdd#(e,1)) lv_exp = {1'b0,lv_exponent};
        Int#(e) lv_original_exponent;

        //`ifdef verbose $display("sign = %b exponent = %h mantissa = %h zero_flag = %b invalid_flag = %b inifnity: %b denormal %b", lv_sign, lv_exponent, lv_mantissa, lv_zero, lv_invalid, lv_infinity,lv_denormal); `endif
        
        if( lv_e_val ==8)
            lv_original_exponent = unpack(truncate(lv_exp - 127));  // removing the bias
        else
            lv_original_exponent = unpack(truncate(lv_exp - 1023));  // removing the bias
        //`ifdef verbose $display("lv_original_exponent : %d flags: %b",lv_original_exponent,flags);`endif
        Bit#(`ELEN) final_result = 0;
		Bit#(TAdd#(m, `ELEN)) final_man = {'0,1'b1,lv_mantissa}; //parametrised for 'm' value
       if(lv_zero == 1)
           final_result = 0;
       else if(lv_denormal == 1 || (lv_original_exponent <= -1 && (lv_infinity|lv_invalid) == 0)) begin
         if(lv_sign==1 && convert_unsigned==1 && ((lv_original_exponent==-1 && (rmm||(rne && lv_manzero==1))) || (lv_original_exponent<=-1 &&rdn)))
             lv_invalid = 1;
         else
           lv_inexact = 1;
           if(lv_sign == 0 && rup)
               final_result = 1;
           else if(rdn && lv_sign == 1 && convert_unsigned == 0)
               final_result = '1;
           else if(lv_original_exponent == -1 && (rmm||(rne && lv_manzero == 1)))begin
               if(lv_sign == 0)
                   final_result = 1;
               else if(convert_unsigned == 0)
                   final_result = '1;
               else
                   final_result = 0;
           end
           else
               final_result = 0;
       end
        else if(convert_long == 0) 
	begin         //FCVT.W.S FCVT.WU.S or  //FCVT.W.D FCVT.WU.D
            if(convert_unsigned == 0) //FCVT.W.S or //FCVT.W.D
	    begin 
                Bit#(31) all_ones = '1;
                if(lv_infinity == 1 || lv_invalid == 1) 
		begin
                   final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(32'h80000000)) : zeroExtend(all_ones); 
                end
                else if(lv_original_exponent < 'd31) 
		begin
                   final_man = final_man << lv_original_exponent;
                   Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                   final_result = signExtend(y);
                   lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value 
                   to_round = True;
                end
               else if(lv_original_exponent >= 'd31) 
	       begin
                   //`ifdef verbose $display("Overflow");`endif
                  // lv_overflow = 1;
                    lv_invalid = 1;
                    if(lv_sign == 0)
                    final_result = zeroExtend(all_ones);
                    else 
		    begin
                       if(lv_original_exponent == 'd31 && lv_manzero == 0)
                       lv_invalid = 0 ;        //Since we are exactly representing the number? 
                       final_result = signExtend(32'h80000000);
                    end
               end
            end
            else begin     //FCVT.WU.S or //FCVT.WU.D
               Bit#(32) all_ones = '1;
               if(lv_infinity == 1 || lv_invalid == 1)
                   final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
               else if(lv_original_exponent < 'd32) begin
                   final_man = final_man << lv_original_exponent;
                   Bit#(32) y = final_man[lv_m_val+31:lv_m_val]; //parametrised for 'm' value 
                   final_result = signExtend(y);
                   lv_mantissa = final_man[lv_m_val-1:0]; //parametrised for 'm' value
                   to_round = True;
               end
               else if(lv_original_exponent >= 'd32) begin
                   //`ifdef verbose $display("Overflow");`endif
                   //lv_overflow = 1;
                     lv_invalid = 1;
                   if(lv_sign == 0)
                    final_result = signExtend(all_ones);
                   else
                    final_result = '0;
               end
            end
        end
        `ifdef RV64
        else begin
            if(convert_unsigned == 0) begin //FCVT.L.S or //FCVT.L.D
                Bit#(63) all_ones = '1;
               if(lv_infinity == 1 || lv_invalid == 1)
                   final_result = (lv_sign==1) ?(lv_invalid==1? zeroExtend(all_ones) : signExtend(64'h8000000000000000)) : zeroExtend(all_ones); 
               else if(lv_original_exponent < 'd63) begin
                   final_man = final_man << lv_original_exponent;
                   //`ifdef verbose $display("final_man : %b",final_man);`endif
                   Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                   final_result = zeroExtend(y);
                   lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                   to_round = True;
               end
               else if(lv_original_exponent >= 'd63) begin
                   //`ifdef verbose $display("Overflow");`endif
                   //lv_overflow = 1;
                   lv_invalid = 1;
                   if(lv_sign == 0)
                    final_result = zeroExtend(all_ones);
                   else begin
                       if(lv_original_exponent == 'd63 && lv_manzero == 0 )
                           lv_invalid = 0;  //Since we are exactly representing the input number
                    final_result = signExtend(64'h8000000000000000);
                   end
               end
            end
            else begin     //FCVT.LU.S or //FCVT.LU.D
               Bit#(64) all_ones = '1;
               if(lv_infinity == 1 || lv_invalid == 1)
                   final_result = (lv_sign==1) ? (lv_invalid==1? signExtend(all_ones) : '0) : signExtend(all_ones); 
               else if(lv_original_exponent < 'd64) begin
                   final_man = final_man << lv_original_exponent;
                   Bit#(64) y = final_man[lv_m_val+63:lv_m_val];  //parametrised for 'm' value
                   final_result = zeroExtend(y);
                   lv_mantissa = final_man[lv_m_val-1:0];  //parametrised for 'm' value
                   to_round = True;
               end
               else if(lv_original_exponent >= 'd64) begin
                   //`ifdef verbose $display("Overflow");`endif
                   //lv_overflow = 1;
                     lv_invalid = 1;
                   if(lv_sign == 0)
                    final_result = signExtend(all_ones);
                   else
                    final_result = '0;
               end
            end
        end
        `endif 

		  bit lv_guard = lv_mantissa[lv_m_val-1];	        //MSB of the already shifted mantissa is guard bit //parametrised for 'm' value
    	bit lv_round = lv_mantissa[lv_m_val-2];	        //next bit is round bit //parametrised for 'm' value
    	bit lv_sticky = |(lv_mantissa<<2);		//remaining bits determine the sticky bit
	    bit lv_round_up = 0;
    	bit lv_inexact1 = lv_guard | lv_round | lv_sticky;
      if(to_round) begin
	      if(rounding_mode == Rnd_Nearest_Even) 		
          lv_round_up = lv_guard & (final_result[0] | lv_round | lv_sticky);	//Round to nearest ties to even
	      else if(rmm) 
          lv_round_up = lv_guard; //& (lv_round | lv_sticky | ~lv_sign);			//Round to nearest ties to max magnitude
	      else if(rdn) 
          lv_round_up = lv_inexact1 & (lv_sign);								//Round down to -infinity
	      else if(rup) 
          lv_round_up = lv_inexact1 & (~lv_sign);								//Round up to +infinity
        
        lv_inexact = lv_inexact | lv_inexact1;
        if(lv_round_up == 1) begin //Should set the overflow flag here right? 
          lv_invalid = 1;
				  if(convert_long == 0 && convert_unsigned == 0 && lv_original_exponent == 30 && 
                                                          final_result[30:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
                                                          if ( lv_e_val == 8)
            
                                                            final_result = 'h7fffffff; //parametrised for 'e' value
                                                        else
                                                            final_result = 64'h7fffffff; //parametrised for 'e' value

          else if(convert_long == 0 && convert_unsigned == 1 && lv_original_exponent == 31 && final_result[31:0] == '1 && lv_sign == 0)
            
            if(lv_e_val ==8)
                final_result = '1; //parametrised for 'e' value
            else
                final_result = 64'hffffffffffffffff; //Should verify again //parametrised for 'e' value
        `ifdef RV64
          else if(convert_long == 1 && convert_unsigned == 0 && lv_original_exponent == 62 && final_result[62:0] == '1 && lv_sign == 0)  //Overflow..  Beyond representable number after rounding
            final_result = 64'h7fffffffffffffff;
          else if(convert_long == 1 && convert_unsigned == 1 && lv_original_exponent == 63 && final_result[63:0] == '1 && lv_sign == 0)
            final_result = 64'hffffffffffffffff;                
        `endif
          else begin
            lv_invalid = 0;
            final_result = final_result + 1;
            if(convert_long == 0 && final_result[31]==1)
              final_result = signExtend(final_result[31:0]);
          end
        end
        //`ifdef verbose $display("rounding_mode == %b",rounding_mode);`endif
	//	    `ifdef verbose $display("round_up = %b", lv_round_up);`endif

			  if(convert_unsigned == 0 && lv_sign == 1)begin		//Negating the output if floating point number is negative and converted to signed word/long
				  final_result = ~final_result + 1;
          if(convert_long == 0 && final_result[31] == 1)
            final_result = signExtend(final_result[31:0]);
	//			  `ifdef verbose $display("Negating output final_result : %b", final_result);`endif
			  end
        else if(convert_unsigned == 1 && lv_sign == 1) begin
				  final_result = 0;
          lv_invalid = 1;
        end
		  end
      if((lv_invalid|lv_infinity) == 1) begin  //What about Quiet NaN?? What does the Spec Say?
        lv_overflow = 0;
        lv_inexact = 0;
      end
      Bit#(5) fflags={lv_invalid|lv_infinity,1'b0,lv_overflow,1'b0,lv_inexact};
      
        rg_stage_out[0] <= Floating_output{
                            final_result: final_result,
                            fflags: fflags};
        rg_stage_valid[0] <= 1;
    endmethod

    method ReturnTypeInt#(`ELEN) receive();
      return ReturnTypeInt{valid:rg_stage_valid[valueOf(nos)-1],value:rg_stage_out[valueOf(nos)-1].final_result,ex:unpack(rg_stage_out[valueOf(nos)-1].fflags)};
    endmethod 
endmodule // sp/dp to int

(*synthesize*)
module mk_fpu_convert_dp_to_int_instance(Ifc_fpu_sp_dp_to_int#(11,52,`STAGES_FCONV));
    let ifc();
    mk_fpu_sp_dp_to_int _temp(ifc);
    return (ifc);
 endmodule

(*synthesize*)
module mk_fpu_convert_sp_to_int_instance(Ifc_fpu_sp_dp_to_int#(8,23,`STAGES_FCONV));
    let ifc();
    mk_fpu_sp_dp_to_int _temp(ifc);
    return (ifc);
 endmodule


///////////////////////////////
/// int to sp
//////////////////////////////
interface Ifc_fpu_int_to_sp;
    method Action start(Bit#(64) inp_int, Bit#(1) unsigned_bit, Bit#(1) long, Bit#(3) rounding_mode);
    method ReturnType#(8,23) receive();
endinterface

    function Bit#(37) roundFunc_sp(Bit#(n) unrounded, Bit#(8) expo, Bit#(3) rounding_mode)
          provisos(
                   Add#(a__,32,n)
                  );
           let nInd = valueOf(n);
           bit guard  = unrounded[nInd-25];
           bit round  = unrounded[nInd-26];
           bit sticky = 0;
           bit sign = unrounded[nInd-1];
           Bit#(8) local_expo = expo;
           Bit#(TSub#(n,26)) sticky_check = unrounded[nInd-27:0];  
           if(sticky_check != '0)
               sticky = 1;
           bit inexact  = (guard | round | sticky);
           bit lv_roundup = 0;
           Bit#(25) lv_man = {2'b0,unrounded[nInd-2:nInd-24]};

	RoundMode rm = unpack(rounding_mode);
           if(rm == Rnd_Nearest_Even) 
		        lv_roundup = guard & (unrounded[nInd-24] | round | sticky);
	       else if (rm == Rnd_Nearest_Away_Zero)
		        lv_roundup = guard; //& (round | sticky | ~sign);
	       else if (rm == Rnd_Plus_Inf)
		        lv_roundup = (guard | round | sticky) & (~sign);
	       else if (rm == Rnd_Minus_Inf)
		        lv_roundup = (guard | round | sticky) & (sign);
           if(lv_roundup == 1)
           lv_man = lv_man + 1;
           if(lv_man[23] == 1) begin
               local_expo = local_expo + 1;
           end
           let fflags = {1'b0,1'b0,1'b0,1'b0,inexact};
        return {fflags,sign,local_expo,lv_man[22:0]};
    endfunction
   
    function Bit#(37) fcvt_s_w_l (Bit#(n) inp, Bit#(1) unsigned_bit, Bit#(3) rounding_mode)
        provisos(
                Add#(a__,32,n),
                Log#(n,logN),
                Add#(b__,logN,8),
                Add#(c__, logN, TLog#(TAdd#(1, n)))
                );
           let nInd = valueOf(n);
           Bool ubit = (unsigned_bit == 1);
           Bit#(1) lv_sign = ubit? 0 : inp[nInd-1];
           Bool sbit = (lv_sign == 1);
           Bit#(7)  bias = '1;
           Bit#(8)  expo = zeroExtend(bias) + fromInteger(nInd-1);
           if(sbit)
               inp = ~inp + 1;
           Bit#(logN) lv_zeros = truncate(pack(countZerosMSB(inp)));
           inp = inp << lv_zeros;
           expo = expo - zeroExtend(pack(lv_zeros));
           Bit#(TSub#(n,1)) inpS = inp[nInd-2:0];
           Bit#(n) inp_temp = {lv_sign,inpS};
           Bit#(37) res = roundFunc_sp(inp_temp, expo,  rounding_mode);
           return res;
    endfunction

(*synthesize*)
module mk_fpu_int_to_sp(Ifc_fpu_int_to_sp);
    Wrapper3#(Bit#(32), Bit#(1), Bit#(3),Bit#(37)) fcvt_s_wwu <- mkUniqueWrapper3(fcvt_s_w_l);
    Wrapper3#(Bit#(64), Bit#(1), Bit#(3),Bit#(37)) fcvt_s_llu <- mkUniqueWrapper3(fcvt_s_w_l);
    // Reg#(Floating_output#(`floating_output_sp_width)) reslt <- mkReg(unpack(0));
    Vector#(`STAGES_FCONV_INT_TO_FP,Reg#(Floating_output#(`floating_output_sp_width))) rg_stage_out <- replicateM(mkReg(unpack(0)));
    Vector#(`STAGES_FCONV_INT_TO_FP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= `STAGES_FCONV_INT_TO_FP -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
    method Action start(Bit#(64) inp_int, Bit#(1) unsigned_bit, Bit#(1) long, Bit#(3) rounding_mode);
//	      `logLevel( tb, 0, $format("Long bit %h",long))
//        `ifdef verbose $display($time,"\tGiving inputs: %h unsigned %b long %b rounding %b", inp_int, unsigned_bit, long, rounding_mode); `endif
		  Floating_output#(`floating_output_sp_width) wr_final_out=?;
        if((inp_int == 0 && long==1) || (inp_int[31:0] == 0 && long == 0))
                    wr_final_out = Floating_output{ final_result : 32'b0,
                                                     fflags       : 5'b0
                                           } ;
        else if(long == 0) 
	begin
		Bit#(32) inp32 = truncate(inp_int);
//		`ifdef verbose $display("inp_int : %b",inp32); `endif
		Bit#(1) lv_sign = inp32[31];
		if(unsigned_bit == 0) 
		begin
                	if((inp32 & 'h7fffffff) == 0) 
			begin
				Bit#(32) res = lv_sign==1? {1'b1,8'h9e,'0} : '0;
				wr_final_out = Floating_output{final_result : res,fflags : 0};
			end
			else 
			begin
				Bit#(37) ressw <- fcvt_s_wwu.func(inp32,unsigned_bit,rounding_mode);
				wr_final_out = Floating_output{final_result : (ressw[31:0]),fflags : ressw[36:32]};
			end
		end
		else 
		begin
	                Bit#(37) res <- fcvt_s_wwu.func(inp32,unsigned_bit,rounding_mode);
                	wr_final_out = Floating_output{final_result : (res[31:0]),fflags : res[36:32]};
		end          
	end
	else 
	begin
		Bit#(37) res <- fcvt_s_llu.func(inp_int,unsigned_bit,rounding_mode);
		wr_final_out = Floating_output{final_result : res[31:0], fflags: res[36:32]};
        end
        
        rg_stage_out[0] <= wr_final_out;
        rg_stage_valid[0] <= 1;
        // reslt <= wr_final_out;
        // return reslt;
    endmethod
    method ReturnType#(8,23) receive();
        let x = unpack(rg_stage_out[`STAGES_FCONV_INT_TO_FP-1].final_result);
        x = canonicalize(x);
        return ReturnType{valid:rg_stage_valid[`STAGES_FCONV_INT_TO_FP-1],value:x,ex:unpack(rg_stage_out[`STAGES_FCONV_INT_TO_FP-1].fflags)};
    endmethod 
endmodule

//////////////////////////////
/// int to dp
/////////////////////////////
function Bit#(m) zeroExtendLSB(Bit#(n) value)
    provisos(Add#(a__, n, m));

    Bit#(m) resp = 0;
    resp[valueOf(m)-1:valueOf(m)-valueOf(n)] = value;
    return resp;
endfunction

function Bit#(m) truncateLSB(Bit#(n) value);
    return value[valueOf(n)-1:valueOf(n)-valueOf(m)];
endfunction
//
interface Ifc_fpu_int_to_dp;
    method Action start(Bit#(64) inp_int, Bit#(1) unsigned_bit, Bit#(1) long, Bit#(3) rounding_mode);
    method ReturnType#(11,52) receive();
endinterface

    function Bit#(69) roundFunc_dp(Bit#(64) unrounded, Bit#(11) expo, Bit#(3) rounding_mode);
        RoundMode x = unpack(rounding_mode);
        bit guard  = unrounded[10];
        bit round  = unrounded[9];
        bit sticky = 0;
        bit sign = unrounded[63];
        Bit#(11) local_expo = expo;
        Bit#(9) sticky_check = unrounded[8:0];  
        if(sticky_check != '0)
            sticky = 1;
        bit inexact  = (guard | round | sticky);
        bit lv_roundup = 0;
        Bit#(54) lv_man = {2'b0,unrounded[62:11]};
    
        if(x == Rnd_Nearest_Even)
            lv_roundup = guard & (unrounded[11] | round | sticky);
        else if (x == Rnd_Nearest_Away_Zero)
            lv_roundup = guard; //& (round | sticky | ~sign);
        else if (x == Rnd_Plus_Inf)
            lv_roundup = (guard | round | sticky) & (~sign);
        else if (x == Rnd_Minus_Inf)
            lv_roundup = (guard | round | sticky) & (sign);
        if(lv_roundup == 1)
        lv_man = lv_man + 1;
        if(lv_man[52] == 1) begin
            local_expo = local_expo + 1;
        end
        let fflags = {1'b0,1'b0,1'b0,1'b0,inexact};
        return {fflags,sign,local_expo,lv_man[51:0]};
    endfunction

(*synthesize*)
 module mk_fpu_int_to_dp(Ifc_fpu_int_to_dp);
    Vector#(`STAGES_FCONV_INT_TO_FP,Reg#(Floating_output#(`floating_output_dp_width))) rg_stage_out <- replicateM(mkReg(unpack(0)));
    Vector#(`STAGES_FCONV_INT_TO_FP,Reg#(Bit#(1))) rg_stage_valid <- replicateM(mkDReg(0));
      rule rl_pipeline;
         for(Integer i = 1 ; i <= `STAGES_FCONV_INT_TO_FP -1 ; i = i+1)
         begin
            rg_stage_out[i] <= rg_stage_out[i-1];
            rg_stage_valid[i] <= rg_stage_valid[i-1];
         end
      endrule
    method Action start(Bit#(64) inp_int, Bit#(1) unsigned_bit, Bit#(1) long, Bit#(3) rounding_mode);
        Floating_output#(`floating_output_dp_width) wr_final_out=?;
        if((inp_int == 0 && long==1) || (inp_int[31:0] == 0 && long == 0))
                    wr_final_out = Floating_output{ final_result : 64'b0,
                                                     fflags       : 5'b0
                                           } ;
        else if(long == 0) begin
                Bit#(32) inp32 = inp_int[31:0];
                Bool ubit = (unsigned_bit == 1);
                Bit#(1) lv_sign = ubit? 0 : inp32[31];
                Bool sbit = (lv_sign==1);
                Bit#(10) bias = '1;
                Bit#(11) expo = zeroExtend(bias) + 31;
                if(sbit)
                    inp32 = ~inp32+1;
                Bit#(5) lv_zeros = truncate(pack(countZerosMSB(inp32)));
                inp32 = inp32 << lv_zeros;
                expo = expo - zeroExtend(lv_zeros);
                Bit#(52) mantissa = zeroExtendLSB(inp32[30:0]);
                Bit#(64) res = {lv_sign,expo,mantissa};
                    wr_final_out = Floating_output {
                                                      final_result : res,
                                                      fflags       : 0
                                                    };
            end
            else begin
//                 `ifdef verbose $display("inp_int : %b",inp_int); `endif
                Bool ubit = (unsigned_bit == 1);
                Bit#(1) lv_sign = ubit? 0 : inp_int[63];
                Bool sbit = (lv_sign==1);
                Bit#(10) bias = '1;
                Bit#(11) expo = zeroExtend(bias) + 63;
                if(sbit)
                    inp_int = ~inp_int + 1;
                Bit#(6) lv_zeros = truncate(pack(countZerosMSB(inp_int)));
                inp_int = inp_int << lv_zeros;
                expo = expo - zeroExtend(lv_zeros);
                Bit#(69) res = roundFunc_dp({lv_sign,inp_int[62:0]},expo,rounding_mode);
                wr_final_out = Floating_output {
                                                    final_result : res[63:0],
                                                    fflags       : res[68:64]
                                                };
            end
        rg_stage_out[0] <= wr_final_out;
        rg_stage_valid[0] <= 1;
    endmethod
    method ReturnType#(11,52) receive();
        let x = unpack(rg_stage_out[`STAGES_FCONV_INT_TO_FP-1].final_result);
        x = canonicalize(x);
        return ReturnType{valid:rg_stage_valid[`STAGES_FCONV_INT_TO_FP-1],value:x,ex:unpack(rg_stage_out[`STAGES_FCONV_INT_TO_FP-1].fflags)};
    endmethod 
endmodule
endpackage

